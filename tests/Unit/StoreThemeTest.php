<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Course;
use Crypt;

class StoreThemeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGet(){
        $user = User::join('rols', 'rols.id', '=', 'users.rol_id')->where('rols.name', '=', 'teacher')->first();

        $this->actingAs($user)
            ->assertAuthenticated($guard = null)
             ->get('/COMPILADORES/temas/crear/PROFESOR/Mg==/NA==')
            ->assertOk()
            ->assertViewIs('themes.create');
    }

    public function testPost(){
        $user = User::select('users.*', 'rols.name as rol_name')->join('rols', 'rols.id', '=', 'users.rol_id')->where('rols.name', '=', 'teacher')->first();

        $course = Course::where('user_id', '=', $user->id)->where('name', '=', 'COMPILADORES')->first();

        $this->actingAs($user)
              ->assertAuthenticated($guard = null)
              ->json('POST', '/temas/crear/', ['name' => 'tema1 prueba automática',
                                               'description' => 'descripcion',
                                               'user_id' => Crypt::encrypt($user->id),
                                               'course_id' => Crypt::encrypt($course->id)
                                             ])
             ->assertStatus(302)
             ->assertRedirect('/COMPILADORES/temas/PROFESOR/Mg==/NA==');

    }
}
