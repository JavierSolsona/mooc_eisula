@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
 <div class="carousel-inner">
   @for ($i=0; $i < count($sliders); $i++)
     @if ($sliders[$i]->image_route != '')
       <div @if($i === 0) class="carousel-item active" @else class="carousel-item" @endif >
         <img class="d-block w-100" src="{{URL::asset($sliders[$i]->image_route)}}" height="300">
       </div>
     @endif
   @endfor
 </div>
 <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
   <span class="carousel-control-prev-icon" aria-hidden="true"></span>
   <span class="sr-only">Previous</span>
 </a>
 <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
   <span class="carousel-control-next-icon" aria-hidden="true"></span>
   <span class="sr-only">Next</span>
 </a>
</div>

<div class="separator">
  <div class="ula-background-color width-parent">
      <h1 class="margin-left letter-color over-hidden">Últimos Cursos</h1>
  </div>
</div>

<div class="container">

     @if(Session::has('message'))
       <div class="separator">
         <div class="row  alert alert-success">
           <h4>{{Session::get('message')}}</h4>
         </div>
       </div>
     @elseif(Session::has('error_message'))
       <div class="separator">
         <div class="row alert alert-danger">
           <h4>{{Session::get('error_message')}}</h4>
         </div>
       </div>
     @endif

     @for ($i=0; $i < count($courses); $i++)
       @if (($i % 4) == 0)
         <div class="row">
       @endif

       <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 separator">
           <div class="post">
               <div class="post-img-content">
                  <a href="{{route('theme.show', [str_replace(' ', '_', $courses[$i]->name), base64_encode($courses[$i]->id)])}}">
                    <img src="{{URL::asset($courses[$i]->image_route)}}" />
                  </a>
               </div>
               <div class="content word-break-all">
                   <a href="{{route('theme.show', [str_replace(' ', '_', $courses[$i]->name), base64_encode($courses[$i]->id)])}}">
                     {{$courses[$i]->name}}
                   </a>
                   <br>
                   <a href="{{route('theme.show', [str_replace(' ', '_', $courses[$i]->name), base64_encode($courses[$i]->id)])}}">
                     {{$courses[$i]->user_name}}
                   </a>
                    <br>
                   {!!nl2br($courses[$i]->description)!!}
               </div>
           </div>
       </div>

       @if ((($i % 4) == 3) || ($i == (count($courses) -1)))
         </div>
       @endif
     @endfor

</div>
@endsection

@section('js')

@endsection
