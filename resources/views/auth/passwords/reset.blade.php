@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ula-background-color"><div class="letter-color">Recobar Contraseña</div></div>

                <div class="card-body">
                    {!! Form::open(['route' => ['password.request'], 'method' => 'post']) !!}
                        @csrf

                        {!! Form::hidden('token', $token) !!}

                        <div class="form-group row">
                          {!! Form::label('email', 'Correo Electrónico', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                          @php
                              $class_email =    $errors->has('email') ? ' is-invalid' : '' ;
                          @endphp

                            <div class="col-md-6">
                                {!! Form::email('email', '', ['value' => "{{ $email ?? old('email') }}", 'id' => 'email', 'class' => "form-control$class_email", 'required' => 'required']) !!}

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('password', 'Contraseña', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            @php
                                $class_password =    $errors->has('password') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                                {!! Form::password('password', ['id' => 'password', 'class' => "form-control$class_password", 'required' => 'required']) !!}

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                           {!! Form::label('password-confirm', 'Confirmar Contraseña', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                {!! Form::password('password_confirmation', ['id' => 'password-confirm', 'class' => "form-control", 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {!! Form::submit('Recobar Contraseña', ['class' => 'btn btn-primary']); !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection
