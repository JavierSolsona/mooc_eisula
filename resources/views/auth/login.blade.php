@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">

  @if(Session::has('message'))
    <div class="row  alert alert-success">
      <h4>{{Session::get('message')}}</h4>
    </div>
  @elseif(Session::has('error_message'))
    <div class="row alert alert-danger">
      <h4>{{Session::get('error_message')}}</h4>
    </div>
  @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ula-background-color"><div class="letter-color">Ingresar</div></div>

                <div class="card-body">
                    {!! Form::open(['route' => ['login'], 'method' => 'post']) !!}
                        @csrf

                        <div class="form-group row">
                            {!! Form::label('email', 'Correo Electrónico', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            @php
                                $class_email =    $errors->has('email') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                                {!! Form::email('email', '', ['value' => "{{ old('email') }}", 'id' => 'email', 'class' => "form-control$class_email", 'required' => 'required']) !!}

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('password', 'Contraseña', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            @php
                                $class_password =    $errors->has('password') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                                {!! Form::password('password', ['id' => 'password', 'class' => "form-control$class_password", 'required' => 'required']) !!}

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    {!! Form::checkbox('remember', old('remember'), "", ['class' => "form-check-input", 'id' => 'remember']); !!}

                                    {!! Form::label('remember', 'Recordarme', ['class' => 'form-check-label']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                               {!! Form::submit('Ingresar', ['class' => 'btn btn-primary']); !!}

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    ¿Olvidaste tú contraseña?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection
