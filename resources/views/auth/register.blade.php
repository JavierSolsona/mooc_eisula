@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ula-background-color"><div class="letter-color">Registro</div></div>

                <div class="card-body">
                  {!! Form::open(['route' => ['register'], 'files' => true]) !!}
                        @csrf

                        <div class="form-group row">
                          {!! Form::label('name', 'Nombre', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                @php
                                    $class_name =    $errors->has('name') ? ' is-invalid' : '' ;
                                @endphp

                                {!! Form::text('name', '', ['value' => "{{ old('name') }}", 'id' => 'name', 'class' => "form-control$class_name upperCase", 'required' => 'required']) !!}

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                          {!! Form::label('email', 'Correo Electrónico', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                          @php
                              $class_email =    $errors->has('email') ? ' is-invalid' : '' ;
                          @endphp

                            <div class="col-md-6">
                                {!! Form::email('email', '', ['value' => "{{ old('email') }}", 'id' => 'email', 'class' => "form-control$class_email", 'required' => 'required']) !!}

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                          {!! Form::label('country_id', 'País', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                          @php
                              $class_country =    $errors->has('country_id') ? ' is-invalid' : '' ;
                          @endphp

                            <div class="col-md-6">
                                {!! Form::select('country_id', $country, "{{ old('country_id') }}", array('class' => "form-control$class_country", 'id' => 'country_id')) !!}

                                @if ($errors->has('country_id'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('photo', 'Foto', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            @php
                                $class_photo =    $errors->has('photo') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                                {!! Form::file('photo', $attributes = ['id' => 'photo', 'class' => "form-control$class_photo fileinput-filename", 'value' => "{{ old('photo') }}"]) !!}

                                <small><i>La foto debe pesar menos de 3MB.</i></small>
                                @if ($errors->has('photo'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('universityStudent', 'Estudiante Universitario', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            @php
                                $class_universityStudent =    $errors->has('universityStudent') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                              {!! Form::checkbox('universityStudent', old('universityStudent'), "", ['class' => "form-control$class_universityStudent"]); !!}

                                @if ($errors->has('universityStudent'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('universityStudent') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('password', 'Contraseña', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            @php
                                $class_password =    $errors->has('password') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                                {!! Form::password('password', ['id' => 'password', 'class' => "form-control$class_password", 'required' => 'required']) !!}

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                           {!! Form::label('password-confirm', 'Confirmar Contraseña', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                {!! Form::password('password_confirmation', ['id' => 'password-confirm', 'class' => "form-control", 'required' => 'required']) !!}

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {!! Form::submit('Registrarse', ['class' => 'btn btn-primary']); !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection
