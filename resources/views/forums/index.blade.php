@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="row justify-content-center">
       <div class="col-auto-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 navbar-laravel">
         <div id="comment-box" name="comment-box">
           @if (count($comments) == 0)
             <p class="paragraph-margin-top"> <h5>Este foro aún no tiene comentarios se él primero en comentar.</h5> </p>
           @else
             @foreach ($comments as $comment)
               <div class="comment-border-bottom" id="comment{{$comment->id}}" name="comment{{$comment->id}}">
                 <p class="paragraph-margin-bottom"><span><i class="fa fa-calendar"></i> {{date("d-m-Y", strtotime($comment->created_at))." | "}} <i class="fa fa-clock-o"> </i> {{date("H:i", strtotime($comment->created_at))}}</span></p>
                 <p class="paragraph-margin-bottom"> <span id="comment{{$comment->id}}_user"> {{$comment->user_name}} </span> @if ($course->user_id == $comment->user_id) <small>Profesor</small> @endif  </p>
                 <p class="paragraph-margin-bottom" id="comment{{$comment->id}}_comment"> <span> {!!nl2br($comment->comment)!!}</span> </p>

                  @if ($comment->edit == true)
                    <p class="paragraph-margin-bottom"> <span> Editado</span> </p>
                  @endif

                  @if ($owner == 1)
                    <a onclick="form_response_open({{$comment->id}})">Responder </a>
                  @endif

                  @if ($comment->user_id == Auth::user()->id)
                    <a  onclick="form_edit_open({{$comment->id}})">Editar</a>
                    <a  onclick="delete_comment({{$comment->id}})">Eliminar</a>
                  @elseif ($owner == 1)
                    <a  onclick="delete_comment({{$comment->id}})">Eliminar</a>
                  @endif
               </div>
             @endforeach
           @endif
         </div>

         {!! Form::open(['id' => 'forum_comment', 'name' => 'forum_comment']) !!}
            @csrf
            <div class="form-group class-padding-top-information-show">
              <h4>Comentar</h4>
            </div>
            <div class="form-group">
              {!! Form::text('forum_comment_name', Auth::user()->name, ['id' => 'forum_comment_name', 'class' => "form-control upperCase", 'required' => 'required', 'disabled' => 'disabled']) !!}
            </div>
            <div class="form-group">
              {!! Form::textarea('forum_comment_comment', null, ['id' => 'forum_comment_comment', 'class' => "form-control", 'placeholder' => "Escribe tu comentario...", 'rows' => '5']) !!}
            </div>
            {!! Form::hidden('user_id', Crypt::encrypt(Auth::user()->id)) !!}
            {!! Form::hidden('forum_id', Crypt::encrypt($forum->id)) !!}
            <div class="form-group">
              {!! Form::submit('Comentar', ['class' => 'btn btn-primary']); !!}
            </div>
        {!! Form::close() !!}

        {!! Form::open(['id' => 'forum_comment_update', 'name' => 'forum_comment_update', 'style' => 'display: none;']) !!}
           @csrf
           <div class="form-group class-padding-top-information-show">
             <h4>Editar Comentario</h4>
           </div>
           <div class="form-group">
             {!! Form::text('forum_comment_name_update', '', ['id' => 'forum_comment_name_update', 'class' => "form-control upperCase", 'required' => 'required', 'disabled' => 'disabled']) !!}
           </div>
           <div class="form-group">
             {!! Form::textarea('forum_comment_comment_update', null, ['id' => 'forum_comment_comment_update', 'class' => "form-control", 'rows' => '5']) !!}
           </div>
           {!! Form::hidden('comment_id_update', '', ['id' => 'comment_id_update']) !!}
           <div class="form-group">
             {!! Form::submit('Editar', ['class' => 'btn btn-primary']); !!}
             <a class="btn btn-danger" onclick="cancel_update()"> Cancelar</a>
           </div>
       {!! Form::close() !!}

       {!! Form::open(['id' => 'forum_comment_response', 'name' => 'forum_comment_response', 'style' => 'display: none;']) !!}
          @csrf
          <div class="form-group class-padding-top-information-show">
            <h4>Responder </h4>
          </div>
          <div class="form-group">
            {!! Form::text('forum_comment_response_name', Auth::user()->name, ['id' => 'forum_comment_response_name', 'class' => "form-control upperCase", 'required' => 'required', 'disabled' => 'disabled']) !!}
          </div>
          <div class="form-group">
            {!! Form::textarea('forum_comment_response_comment', null, ['id' => 'forum_comment_response_comment', 'class' => "form-control", 'placeholder' => "Escribe tu respuesta...", 'rows' => '5']) !!}
          </div>
          {!! Form::hidden('forum_comment_response_user_id', Crypt::encrypt(Auth::user()->id)) !!}
          {!! Form::hidden('forum_comment_response_forum_id', Crypt::encrypt($forum->id)) !!}
          <div class="form-group">
            {!! Form::submit('Responder', ['class' => 'btn btn-primary']); !!}
            <a class="btn btn-danger" onclick="cancel_response()"> Cancelar</a>
          </div>
      {!! Form::close() !!}
       </div>
     </div>
 </div>
 @endsection

 @section('js')
   <script type="text/javascript">
     var count = JSON.parse('{{count($comments)}}');
   </script>
   <script src="{{ asset("/js/forum_comment.js") }}"></script>
 @endsection
