@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ula-background-color"><div class="letter-color">Crear Recurso</div></div>

                <div class="card-body">
                  {!! Form::open(['route' => ['information.store'], 'files' => true, 'id' => 'InformationForm', 'name' => 'InformationForm']) !!}
                        @csrf

                        <div class="form-group row">
                          {!! Form::label('name', 'Nombre', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                @php
                                    $class_name =    $errors->has('name') ? ' is-invalid' : '' ;
                                @endphp

                                {!! Form::text('name', '', ['value' => "{{ old('name') }}", 'id' => 'name', 'class' => "form-control$class_name upperCase", 'required' => 'required']) !!}

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                          {!! Form::label('type_id', 'Tipo', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                          @php
                              $class_type =    $errors->has('type_id') ? ' is-invalid' : '' ;
                          @endphp

                            <div class="col-md-6">
                                {!! Form::select('type_id', $type, "{{ old('type_id') }}", array('class' => "form-control$class_type", 'id' => 'type_id', 'onchange' => 'changeType()')) !!}

                                @if ($errors->has('type_id'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row"  id="div-file">
                            {!! Form::label('file', 'Archivo', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            @php
                                $class_route =    $errors->has('file') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                                {!! Form::file('file', $attributes = ['id' => 'file', 'class' => "form-control$class_route fileinput-filename", 'value' => "{{ old('file') }}"]) !!}

                                <small><i>El archivo debe pesar menos de 10MB.</i></small>

                                @if ($errors->has('file'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div  id="div-link" class="form-group row">
                            {!! Form::label('link', 'Link', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            @php
                                $class_link =    $errors->has('link') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                                {!! Form::text('link', '', ['value' => "{{ old('link') }}", 'id' => 'link', 'class' => "form-control$class_link"]) !!}

                                @if ($errors->has('link'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {!! Form::hidden('user_id', Crypt::encrypt(Auth::user()->id)) !!}
                        {!! Form::hidden('theme_id', Crypt::encrypt($theme->id)) !!}

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {!! Form::submit('Crear Recurso', ['class' => 'btn btn-primary']); !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script charset="utf8"  src="{{ asset("/js/chanceType.js") }}"></script>
@endsection
