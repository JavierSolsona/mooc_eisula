@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="embed-responsive embed-responsive-21by9">
       <iframe class="embed-responsive-item" src="{{URL::asset($video->route)}}" frameborder="0" allowfullscreen></iframe>
     </div>

</div>
@endsection

@section('js')

@endsection
