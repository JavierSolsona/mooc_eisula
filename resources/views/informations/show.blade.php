@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="row ula-background-color">
         <h3 class="margin-left letter-color width-parent over-hidden">Curso: {{$data->course_name}} -> Tema: {{$data->name}} -> Profesor: {{$data->user_name}}</h3>
     </div>

     <div class="row">
       <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 class-padding-top-information-show">
         <div class="ula-background-color">
             <h2 class="margin-left letter-color width-parent over-hidden">Vídeos</h2>
         </div>
         <div class="table-responsive">
         <table id="tableVideosInformation" class="table table-bordered table-hover">
           <thead class="ula-background-color letter-color">
             <tr class="text-center">
               <th>Nombre</th>
             </tr>
           </thead>
           <tbody>
             @foreach($videos as $video)
               <tr class="text-center">
                 <td>
                   <a href="{{route('information.showVideo', [base64_encode(Auth::user()->id), base64_encode($video->id)])}}" onclick="information_view('{{base64_encode($video->id)}}', '{{base64_encode(Auth::user()->id)}}')">{{$video->name}} </a>
                   @if ($video->view > 0)
                     <i class="fa fa-check" aria-hidden="true"></i>
                   @endif
                 </td>
               </tr>
             @endforeach
           </tbody>
         </table>
         </div>
       </div>

       <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 class-padding-top-information-show">
         <div class="ula-background-color">
             <h2 class="margin-left letter-color width-parent over-hidden">Archivos</h2>
         </div>
         <div class="table-responsive">
         <table id="tableFilesInformation" class="table table-bordered table-hover">
           <thead class="ula-background-color letter-color">
             <tr class="text-center">
               <th>Nombre</th>
             </tr>
           </thead>
           <tbody>
             @foreach($files as $file)
               <tr class="text-center">
                 <td>
                   <a target="_blank" href="{{URL::asset($file->route)}}" onclick="information_view('{{base64_encode($file->id)}}', '{{base64_encode(Auth::user()->id)}}')">{{$file->name}} </a>
                   @if ($file->view > 0)
                     <i class="fa fa-check" aria-hidden="true"></i>
                   @endif
                 </td>
               </tr>
             @endforeach
           </tbody>
         </table>
         </div>
       </div>
     </div>

     <div class="row">
       <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 class-padding-top-information-show">
         <div class="ula-background-color">
             <h2 class="margin-left letter-color width-parent over-hidden">Asignaciones</h2>
         </div>
         <div class="table-responsive">
         <table id="tableAssignmentsInformation" class="table table-bordered table-hover">
           <thead class="ula-background-color letter-color">
             <tr class="text-center">
               <th>Nombre</th>
             </tr>
           </thead>
           <tbody>
             @foreach($assignments as $assignment)
               <tr class="text-center">
                 <td>
                   <a target="_blank" href="{{URL::asset($assignment->route)}}" onclick="information_view('{{base64_encode($assignment->id)}}', '{{base64_encode(Auth::user()->id)}}')">{{$assignment->name}} </a>
                   @if ($assignment->view > 0)
                    <i class="fa fa-check" aria-hidden="true"></i>
                   @endif
                 </td>
               </tr>
             @endforeach
           </tbody>
         </table>
         </div>
       </div>

       <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 class-padding-top-information-show">
         <div class="ula-background-color">
             <h2 class="margin-left letter-color width-parent over-hidden">Enlaces de Interés</h2>
         </div>
         <div class="table-responsive">
         <table id="tableLinksInformation" class="table table-bordered table-hover">
           <thead class="ula-background-color letter-color">
             <tr class="text-center">
               <th>Nombre</th>
             </tr>
           </thead>
           <tbody>
             @foreach($links as $link)
               <tr class="text-center">
                 <td><a target="_blank" href="{{URL::asset($link->route)}}">{{$link->name}} </a></td>
               </tr>
             @endforeach
           </tbody>
         </table>
         </div>
       </div>
     </div>

</div>
@endsection

@section('js')
  <script charset="utf8"  src="{{ asset("/plugins/DataTables-1.10.19/media/js/jquery.dataTables.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
        $('#tableVideosInformation').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });

        $('#tableFilesInformation').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });

        $('#tableAssignmentsInformation').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });

        $('#tableLinksInformation').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
    } );
  </script>
  <script charset="utf8"  src="{{ asset("/js/information_view.js") }}"></script>
@endsection
