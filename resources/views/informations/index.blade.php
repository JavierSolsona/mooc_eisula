@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="row">

        <a class="btn btn-primary btn-style" href="{{route('information.create', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($theme->id)])}}">Crear Recurso</a>

     </div>

     <div class="table-responsive">
     <table id="tableIndexInformation" class="table table-bordered table-hover">
       <thead class="ula-background-color letter-color">
         <tr class="text-center">
           <th>Tema</th>
           <th>Nombre</th>
           <th>Tipo</th>
           <th>Archivo</th>
           <th>Acciones</th>
         </tr>
       </thead>
       <tbody>
         @foreach($informations as $information)
           <tr class="text-center">
             <td>{{$information->theme_name}}</td>
             <td>{{$information->name}}</td>
             <td>{{$information->type_name}}</td>
             <td>
                @if ($information->type_name == "VÍDEO")
                  <a target="_blank" href="{{route('information.showVideo', [base64_encode(Auth::user()->id), base64_encode($information->id)])}}">{{$information->name}} </a>
                @else
                  <a target="_blank" href="{{URL::asset($information->route)}}">{{$information->name}} </a> 
                @endif
             </td>
             <td>
                 <a class="btn btn-primary btn-style-table" href="{{route('information.edit', [str_replace(' ', '_', $information->theme_name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($information->id)])}}">Editar</a>
                 <a class="btn btn-danger btn-style-table" href="{{route('information.delete', [str_replace(' ', '_', $information->theme_name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($information->id)])}}"  onclick="return confirm('¿Seguro que deseas eliminar este recurso?')">Eliminar</a>
             </td>
           </tr>
         @endforeach
       </tbody>
     </table>
     </div>


</div>
@endsection

@section('js')
  <script charset="utf8"  src="{{ asset("/plugins/DataTables-1.10.19/media/js/jquery.dataTables.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
        $('#tableIndexInformation').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
    } );
  </script>
@endsection
