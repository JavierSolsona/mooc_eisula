<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset("/js/jquery.blockUI.js") }}"></script>
    <script src="{{ asset("/js/jquery.validate.min.js") }}"></script>
    <script src="{{ asset("/js/deleteUser.js") }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('css')
</head>
<body>
    <div id="app">
      <div class="ula-background-color">
        <nav class="navbar navbar-expand-md navbar-light ">
            <div class="container">
                <div class="logo-image">
                  <img class="img-responsive" src="{{asset('images/ula.png')}}" width="80" height="90">
                </div>
                <a class="navbar-brand app-name-margin-left" href="{{ url('/') }}">
                  <div class="letter-color">
                    {{ config('app.name', 'Laravel') }}
                  </div>
                </a>

                <div class="custom-user-menu" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('user.manual.index') }}"><div class="letter-color">Manual</div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}"><div class="letter-color">Ingresar</div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}"><div class="letter-color">Registrarse</div></a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <img src="{{asset(Auth::user()->photo)}}" class="circle" alt="User Image" width="40" height="40" /> <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('user.manual.index')}}">
                                        Manual
                                    </a>
                                    <a class="dropdown-item" href="{{ route('user.edit', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)]) }}">
                                        Editar Perfil
                                    </a>
                                    @if(!Auth::user()->hasRole('administrator'))
                                      <a class="dropdown-item" href="{{ route('user.delete', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)]) }}" onclick="return deleteUser();">
                                          Eliminar Cuenta
                                      </a>
                                    @endif
                                    @if(Auth::user()->hasRole('administrator'))
                                      <a class="dropdown-item" href="{{ route('slider.index') }}">
                                          Imágenes
                                      </a>
                                      <a class="dropdown-item" href="{{ route('teacher.create') }}">
                                          Crear Profesor
                                      </a>
                                      <a class="dropdown-item" href="{{ route('backup.create') }}">
                                          Crear Respaldo
                                      </a>
                                      <a href="#" data-toggle="modal" data-target="#ModalBackUpCharge" class="dropdown-item">
                                          Recuperar Respaldo
                                      </a>
                                      <a class="dropdown-item" href="{{ route('user.stats') }}">
                                          Estadísticas
                                      </a>
                                    @elseif(Auth::user()->hasRole('teacher'))
                                      <a class="dropdown-item" href="{{ route('course.index', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)]) }}">
                                          Administrar Cursos
                                      </a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('signedUp.signed', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)]) }}">
                                        Ver Cursos Inscriptos
                                    </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
                <div class="logo-image">
                  <img class="img-responsive" src="{{asset('images/eisula.png')}}" width="80" height="90">
                </div>
            </div>
        </nav>
        <div class="container">
          <div class="row class-padding-bottom">
            <div class="col-3 col-sm-3 col-md-2 col-lg-2 col-xl-2">
              <a class="btn-style-header letter-color" href="{{route('course.showAll')}}">Ver Cursos</a>
            </div>
            <div class="col-9 col-sm-9 col-md-10 col-lg-10 col-xl-10">
              {!! Form::open(['route' => ['course.search']]) !!}
                    @csrf
                <div class="form-row align-items-center">
                  <div class="col-12 col-sm-8 col-md-9 col-lg-10 col-xl-10 my-1">
                    {!! Form::text('search', '', ['id' => 'search', 'class' => "form-control upperCase", 'required' => 'required', 'placeholder' =>'Buscar Curso...']) !!}
                    @if ($errors->has('search'))
                        <span class="invalid-feedback display" role="alert">
                            <strong>{{ $errors->first('search') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="col-auto my-1">
                    {!! Form::submit('Buscar'); !!}
                  </div>
              </div>
              {!! Form::close() !!}
            </div>
          </div>

        </div>
      </div>

      @if(Auth::check() && Auth::user()->hasRole('administrator'))
        <div class="modal" id="ModalBackUpCharge" role="dialog" tabindex="-1">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title"> Carga Archivo para respaldo </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body">
                {!! Form::open(['route' => ['backup.recover'], 'files' => true]) !!}
                      @csrf
                      <div class="form-group row">
                          @php
                              $class_file =    $errors->has('file') ? ' is-invalid' : '' ;
                          @endphp

                          <div class="col-md-12">
                              {!! Form::file('file', $attributes = ['id' => 'file', 'class' => "form-control$class_file fileinput-filename", 'value' => "{{ old('file') }}", 'required' => 'required']) !!}

                              @if ($errors->has('file'))
                                  <span class="invalid-feedback display" role="alert">
                                      <strong>{{ $errors->first('file') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                      <div class="form-group row mb-0">
                          <div class="col-md-12 offset-md-4">
                              {!! Form::submit('Cargar Respaldo', ['class' => 'btn btn-primary']); !!}
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                          </div>
                      </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      @endif

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer class="footer">
      <div class="ula-background-color text-center footer-height">
        <div class="vertical-center-footer letter-color">
            <h5>{{ config('app.name', 'Laravel') }} {{date('Y')}} . Todos los derechos reservados</h5>
        </div>
      </div>
    </footer>
    @yield('js')
</body>
</html>
