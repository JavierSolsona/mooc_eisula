@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="row">
        <a class="btn btn-primary btn-style" href="{{route('theme.create', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($course->id)])}}">Crear Tema</a>
     </div>

     <div class="table-responsive">
     <table id="tableIndexTheme" class="table table-bordered table-hover">
       <thead class="ula-background-color letter-color">
         <tr class="text-center">
           <th>Curso</th>
           <th>Nombre</th>
           <th>Descripción</th>
           <th>Acciones</th>
         </tr>
       </thead>
       <tbody>
         @foreach($themes as $theme)
           <tr class="text-center">
             <td>{{$theme->course_name}}</td>
             <td>{{$theme->name}}</td>
             <td class="word-break-all">
               @if ($theme->description == "")
                 Sin descripción
               @else
                 {!!nl2br($theme->description)!!}
               @endif
             </td>
             <td>
              <a class="btn btn-primary btn-style-table" href="{{route('theme.edit', [str_replace(' ', '_', $theme->course_name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])}}">Editar</a>
              <a class="btn btn-primary btn-style-table" href="{{route('forum.index', [str_replace(' ', '_', $theme->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])}}">Foro</a>
              <a class="btn btn-primary btn-style-table" href="{{route('information.index', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])}}">Administrar Recursos</a>
              <a class="btn btn-danger btn-style-table" href="{{route('theme.delete', [str_replace(' ', '_', $theme->course_name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])}}"  onclick="return confirm('¿Seguro que deseas eliminar este tema?')">Eliminar</a>
             </td>
           </tr>
         @endforeach
       </tbody>
     </table>
     </div>


</div>
@endsection

@section('js')
  <script charset="utf8"  src="{{ asset("/plugins/DataTables-1.10.19/media/js/jquery.dataTables.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
        $('#tableIndexTheme').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
    } );
  </script>
@endsection
