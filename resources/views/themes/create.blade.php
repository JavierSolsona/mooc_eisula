@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ula-background-color"><div class="letter-color">Crear Tema</div></div>

                <div class="card-body">
                  {!! Form::open(['route' => ['theme.store']]) !!}
                        @csrf

                        <div class="form-group row">
                          {!! Form::label('name', 'Nombre', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                @php
                                    $class_name =    $errors->has('name') ? ' is-invalid' : '' ;
                                @endphp

                                {!! Form::text('name', '', ['value' => "{{ old('name') }}", 'id' => 'name', 'class' => "form-control$class_name upperCase", 'required' => 'required']) !!}

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                          {!! Form::label('description', 'Descripción', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                @php
                                    $class_description =    $errors->has('description') ? ' is-invalid' : '' ;
                                @endphp

                                {!! Form::textarea('description', null, ['id' => 'description', 'class' => "form-control", 'rows' => '5']) !!}

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {!! Form::hidden('user_id', Crypt::encrypt(Auth::user()->id)) !!}
                        {!! Form::hidden('course_id', Crypt::encrypt($course->id)) !!}

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {!! Form::submit('Crear Tema', ['class' => 'btn btn-primary']); !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection
