@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

   <div class="row">
     @if (Auth::check())
       @if (Auth::user()->hasRole('teacher') && $course->user_id == Auth::user()->id)
       @elseif ($course->signed == 0)
         <button type="button" name="button" class="btn btn-primary btn-style-table" onclick="signedUp2({{$course->id}}, {{Auth::user()->id}})">Inscribir</button>
       @else
         <button type="button" name="button" class="btn btn-danger btn-style-table" onclick="remove2({{$course->id}}, {{Auth::user()->id}})">Retirarse</button>
       @endif
     @endif
   </div>

  <div class="table-responsive">
  <table id="tableShowTheme" class="table table-bordered table-hover">
    <thead class="ula-background-color letter-color">
      <tr class="text-center">
        <th>Tema</th>
        <th>Descripción</th>
        <th>Foro</th>
      </tr>
    </thead>
    <tbody>
      @foreach($themes as $theme)
        <tr class="text-center">
          <td>
            @if (Auth::check())
              @if ($theme->signed == 0)
                {{$theme->name}}
              @else
                <a href="{{route('information.show', [str_replace(' ', '_', $theme->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])}}">{{$theme->name}} </a>
              @endif
            @else
              {{$theme->name}}
            @endif
          </td>
          <td class="word-break-all">
            @if ($theme->description == "")
              Sin descripción
            @else
              {!!nl2br($theme->description)!!}
            @endif
          </td>
          <td>
            @if (Auth::check())
              @if ($theme->signed == 0)
                @if ($theme->course_owner == Auth::user()->id)
                  <a class="btn btn-primary btn-style-table" href="{{route('forum.index', [str_replace(' ', '_', $theme->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])}}">Foro</a>
                @else
                  Debes estar inscrito para ver entrar al foro.
                @endif
              @else
                <a class="btn btn-primary btn-style-table" href="{{route('forum.index', [str_replace(' ', '_', $theme->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])}}">Foro</a>
              @endif
            @else
              Debes iniciar sesión y estar inscrito para ver entrar al foro.
            @endif
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  </div>


</div>

@endsection

@section('js')
  <script charset="utf8"  src="{{ asset("/plugins/DataTables-1.10.19/media/js/jquery.dataTables.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
        $('#tableShowTheme').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
    } );
  </script>
  <script charset="utf8"  src="{{ asset("/js/signed.js") }}"></script>
@endsection
