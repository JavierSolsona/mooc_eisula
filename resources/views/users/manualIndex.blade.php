@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="table-responsive">
     <table id="tableManual" class="table table-bordered table-hover">
       <thead class="ula-background-color letter-color">
         <tr class="text-center">
           <th>Tutorial</th>
         </tr>
       </thead>
       <tbody>
         <tr class="text-center">
           <td><a href="{{route('user.manual.show',['buscar'])}}">Buscar Cursos</a></td>
         </tr>
         @if (Auth::check())
           @if(Auth::user()->hasRole('student'))
             <tr class="text-center">
               <td><a href="{{route('user.manual.show',['editarPerfil'])}}">Editar Perfil</a></td>
             </tr>
             <tr class="text-center">
               <td><a href="{{route('user.manual.show',['inscrip'])}}">Inscripción en Cursos</a></td>
             </tr>
             <tr class="text-center">
               <td><a href="{{route('user.manual.show',['retiro'])}}">Retiro de Cursos</a></td>
             </tr>
             <tr class="text-center">
               <td><a href="{{route('user.manual.show',['acceso'])}}">Acceso a Información</a></td>
             </tr>
             <tr class="text-center">
               <td><a href="{{route('user.manual.show',['foro'])}}">Foros</a></td>
             </tr>
           @endif
       @else
         <tr class="text-center">
           <td><a href="{{route('user.manual.show',['registro'])}}">Registrarse</a></td>
         </tr>
         <tr class="text-center">
           <td><a href="{{route('user.manual.show',['ingresar'])}}">Ingresar</a></td>
         </tr>
        @endif
       </tbody>
     </table>
     </div>
</div>
@endsection

@section('js')
  <script charset="utf8"  src="{{ asset("/plugins/DataTables-1.10.19/media/js/jquery.dataTables.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
        $('#tableManual').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
    } );
  </script>
@endsection
