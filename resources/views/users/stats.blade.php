@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

    <div class="row ula-background-color">
        <h2 class="margin-left letter-color width-parent over-hidden">Estadísticas</h2>
    </div>

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="row class-padding-top-information-show">
       <div class="col-auto-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
        <h5>Estudiantes: {{$students_number}}</h5>
       </div>
       <div class="col-auto-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
         <h5>Profesores: {{$teachers_number}}</h5>
       </div>
     </div>

     <div class="row class-padding-top-information-show">
       <div class="col-auto-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
        <h5>Cursos: {{$courses_number}}</h5>
       </div>
       <div class="col-auto-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
         <h5>Promedio de estudiantes por curso: {{$signed_up_average}}</h5>
       </div>
     </div>

     <div class="class-padding-top-information-show">
       <div class="row ula-background-color">
           <h4 class="margin-left letter-color width-parent over-hidden">Cursos por profesor</h4>
       </div>
     </div>

     <div class="row">
       <div class="table-responsive class-padding-top-information-show">
       <table id="tableCoursesByTeacher" class="table table-bordered table-hover">
         <thead class="ula-background-color letter-color">
           <tr class="text-center">
             <th>Profesor</th>
             <th>Cursos</th>
           </tr>
         </thead>
         <tbody>
           @foreach($teachers_number_courses as $row)
             <tr class="text-center">
               <td>
                 {{$row->name}}
               </td>
               <td>
                 {{$row->couses}}
               </td>
             </tr>
           @endforeach
         </tbody>
       </table>
       </div>
     </div>

     <div class="class-padding-top-information-show">
       <div class="row ula-background-color">
           <h4 class="margin-left letter-color width-parent over-hidden">Estudiantes por curso</h4>
       </div>
     </div>

     <div class="row">
       <div class="table-responsive class-padding-top-information-show">
       <table id="tableStudentsByCourse" class="table table-bordered table-hover">
         <thead class="ula-background-color letter-color">
           <tr class="text-center">
             <th>Curso</th>
             <th>Estudiantes</th>
           </tr>
         </thead>
         <tbody>
           @foreach($courses_number_students as $row)
             <tr class="text-center">
               <td>
                 {{$row->name}}
               </td>
               <td>
                 {{$row->signed}}
               </td>
             </tr>
           @endforeach
         </tbody>
       </table>
       </div>
     </div>

</div>
@endsection

@section('js')
  <script charset="utf8"  src="{{ asset("/plugins/DataTables-1.10.19/media/js/jquery.dataTables.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
        $('#tableCoursesByTeacher').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });

        $('#tableStudentsByCourse').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
    } );
  </script>
@endsection
