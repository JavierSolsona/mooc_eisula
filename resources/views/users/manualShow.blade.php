@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="embed-responsive embed-responsive-21by9">
       <iframe class="embed-responsive-item" src="{{'/videos'. '/'. $name. ".ogg"}}" frameborder="0" allowfullscreen></iframe>
     </div>

</div>
@endsection

@section('js')

@endsection
