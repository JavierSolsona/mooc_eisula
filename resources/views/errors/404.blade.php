@extends('layouts.app')

@section('css')
@endsection

@section('content')

  <div class="container">
    <div class="row justify-content-center">
        <h1> <font color="red">404 {{ $exception->getMessage() }} </font></h1>
    </div>
    <div class="row justify-content-center">
      <a href="{{ route('/') }}" class="btn btn-default"><span>Volver al inicio</span></a>
    </div>

  </div>
@endsection

@section('js')

@endsection
