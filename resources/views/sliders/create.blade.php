@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header ula-background-color"><div class="letter-color">Crear Imagen</div></div>

                <div class="card-body">
                  {!! Form::open(['route' => ['slider.store'], 'files' => true]) !!}
                        @csrf

                        <div class="form-group row">
                            {!! Form::label('image', 'Imagen', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            @php
                                $class_image =    $errors->has('image') ? ' is-invalid' : '' ;
                            @endphp

                            <div class="col-md-6">
                                {!! Form::file('image', $attributes = ['id' => 'image', 'class' => "form-control$class_image fileinput-filename", 'value' => "{{ old('image') }}"]) !!}

                                <small><i>La imagen debe pesar menos de 3MB.</i></small>
                                @if ($errors->has('image'))
                                    <span class="invalid-feedback display" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {!! Form::submit('Crear Imagen', ['class' => 'btn btn-primary']); !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection
