@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="row">
        <a class="btn btn-primary btn-style" href="{{route('course.create', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])}}">Crear Curso</a>
     </div>

     <div class="table-responsive">
     <table id="tableIndexCourse" class="table table-bordered table-hover">
       <thead class="ula-background-color letter-color">
         <tr class="text-center">
           <th>Nombre</th>
           <th>Imagen</th>
           <th>Descripción</th>
           <th>Acciones</th>
         </tr>
       </thead>
       <tbody>
         @foreach($courses as $course)
           <tr class="text-center">
             <td>{{$course->name}}</td>
             <td><img src="{{URL::asset($course->image_route)}}" width="230" height="130" alt="Sin imagen"/></td>
             <td class="word-break-all">
               @if ($course->description == "")
                 Sin descripción
               @else
                 {!!nl2br($course->description)!!}
               @endif
             </td>
             <td>
               <a class="btn btn-primary btn-style-table" href="{{route('course.edit', [str_replace(' ', '_', Auth::user()->name), base64_encode($course->id)])}}">Editar</a>
               <a class="btn btn-primary btn-style-table" href="{{route('theme.index', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])}}">Administrar Temas</a>
               <a class="btn btn-primary btn-style-table" href="{{route('forum.course', [str_replace(' ', '_', $course->name), base64_encode(Auth::user()->id), base64_encode($course->id)])}}">Foro</a>
               <a class="btn btn-danger btn-style-table" href="{{route('course.delete', [str_replace(' ', '_', Auth::user()->name), base64_encode($course->id)])}}"  onclick="return confirm('¿Seguro que deseas eliminar este curso?')">Eliminar</a>
             </td>
           </tr>
         @endforeach
       </tbody>
     </table>
     </div>


</div>
@endsection

@section('js')
  <script charset="utf8"  src="{{ asset("/plugins/DataTables-1.10.19/media/js/jquery.dataTables.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
        $('#tableIndexCourse').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
    } );
  </script>
@endsection
