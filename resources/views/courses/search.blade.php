@extends('layouts.app')

@section('css')
  <link href="{{ asset('/plugins/DataTables-1.10.19/media/css/jquery.dataTables.min.css') }}"  type="text/css" rel="stylesheet">
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="row btn-style">
       <h2 class="ula-background-color letter-color width-parent over-hidden">Resultados para: {{$search}} </h2>
     </div>

     <div class="table-responsive">
     <table id="tableIndexCourseAll" class="table table-bordered table-hover">
       <thead class="ula-background-color letter-color">
         <tr class="text-center">
           <th>Nombre</th>
           <th>Imagen</th>
           <th>Descripción</th>
           <th>Profesor</th>
           <th>Acciones</th>
           @if (Auth::check())
             @if (Auth::user()->hasRole('teacher'))
               <th>Administrar Temas</th>
             @endif
           @endif
         </tr>
       </thead>
       <tbody>
         @foreach($courses as $course)
           <tr class="text-center">
             <td>
               <a href="{{route('theme.show', [str_replace(' ', '_', $course->name), base64_encode($course->id)])}}"> {{$course->name}}</a>
             </td>
             <td><img src="{{URL::asset($course->image_route)}}" width="230" height="130" alt="Sin imagen"/></td>
             <td class="word-break-all">
               @if ($course->description == "")
                 Sin descripción
               @else
                 {!!nl2br($course->description)!!}
               @endif
             </td>
             <td>{{$course->user_name}}</td>
             <td id="{{$course->id}}" name="{{$course->id}}" >
               @if (Auth::check())
                 @if (Auth::user()->hasRole('teacher') && $course->user_id == Auth::user()->id)
                 @elseif ($course->signed == 0)
                   <button type="button" name="button" class="btn btn-primary btn-style-table" onclick="signedUp({{$course->id}}, {{Auth::user()->id}})">Inscribir</button>
                 @else
                   <button type="button" name="button" class="btn btn-danger btn-style-table" onclick="remove({{$course->id}}, {{Auth::user()->id}})">Retirarse</button>
                 @endif
               @else
                 Debes iniciar sesión para  ver las opciones.
               @endif
             </td>
             @if (Auth::check())
               @if (Auth::user()->hasRole('teacher'))
                 <td>
                 @if ($course->user_id == Auth::user()->id)
                   <a class="btn btn-primary btn-style-table" href="{{route('theme.index', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])}}">Administrar Temas</a>
                 @endif
                 </td>
               @endif
             @endif
           </tr>
         @endforeach
       </tbody>
     </table>
     </div>


</div>
@endsection

@section('js')
  <script charset="utf8"  src="{{ asset("/plugins/DataTables-1.10.19/media/js/jquery.dataTables.min.js") }}"></script>
  <script type="text/javascript">
    $(document).ready( function () {
        $('#tableIndexCourseAll').DataTable({
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
        });
    } );
  </script>
  <script charset="utf8"  src="{{ asset("/js/signed.js") }}"></script>
@endsection
