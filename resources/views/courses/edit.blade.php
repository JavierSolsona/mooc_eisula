@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">

     @if(Session::has('message'))
       <div class="row  alert alert-success">
         <h4>{{Session::get('message')}}</h4>
       </div>
     @elseif(Session::has('error_message'))
       <div class="row alert alert-danger">
         <h4>{{Session::get('error_message')}}</h4>
       </div>
     @endif

     <div class="row justify-content-center">
         <div class="col-md-8">
             <div class="card">
                <div class="card-header ula-background-color"><div class="letter-color">Editar Curso</div></div>

                 <div class="card-body">
                   {!! Form::open(['route' => ['course.update'], 'files' => true]) !!}
                         @csrf
                         @method('PUT')

                         <div class="form-group row">
                           {!! Form::label('name', 'Nombre', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                             <div class="col-md-6">
                                 @php
                                     $class_name =    $errors->has('name') ? ' is-invalid' : '' ;
                                 @endphp

                                 {!! Form::text('name', $course->name, ['id' => 'name', 'class' => "form-control$class_name upperCase", 'required' => 'required']) !!}

                                 @if ($errors->has('name'))
                                     <span class="invalid-feedback display" role="alert">
                                         <strong>{{ $errors->first('name') }}</strong>
                                     </span>
                                 @endif
                             </div>
                         </div>

                         <div class="form-group row">
                             {!! Form::label('image', 'Imagen', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                             @php
                                 $class_image =    $errors->has('image') ? ' is-invalid' : '' ;
                             @endphp

                             <div class="col-md-6">
                                 {!! Form::file('image', $attributes = ['id' => 'image', 'class' => "form-control$class_image fileinput-filename", 'value' => "{{ old('image') }}"]) !!}

                                 <small><i>La imagen debe pesar menos de 3MB.</i></small>
                                 @if ($errors->has('image'))
                                     <span class="invalid-feedback display" role="alert">
                                         <strong>{{ $errors->first('image') }}</strong>
                                     </span>
                                 @endif
                             </div>
                         </div>

                         <div class="form-group row">
                           {!! Form::label('description', 'Descripción', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                             <div class="col-md-6">
                                 @php
                                     $class_description =    $errors->has('description') ? ' is-invalid' : '' ;
                                 @endphp

                                 {!! Form::textarea('description', $course->description, ['id' => 'description', 'class' => "form-control", 'rows' => '5']) !!}

                                 @if ($errors->has('description'))
                                     <span class="invalid-feedback display" role="alert">
                                         <strong>{{ $errors->first('description') }}</strong>
                                     </span>
                                 @endif
                             </div>
                         </div>

                         {!! Form::hidden('course_id', Crypt::encrypt($course->id)) !!}

                         <div class="form-group row mb-0">
                             <div class="col-md-6 offset-md-4">
                                 {!! Form::submit('Editar Curso', ['class' => 'btn btn-primary']); !!}
                             </div>
                         </div>
                     {!! Form::close() !!}
                 </div>
             </div>
         </div>
     </div>
 </div>
 @endsection

 @section('js')

 @endsection
