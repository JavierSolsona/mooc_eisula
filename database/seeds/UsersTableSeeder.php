<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert(
     array(
       array(
         'name'        => 'ADMINISTRADOR',
         'email'       => 'administrador@gmail.com',
         'password'    => bcrypt('123456'),
         'rol_id'      => '1',
         'country_id'     => '1',
         'universityStudent' => '0',
         'photo' => 'profile_pictures/user.png',
       ),
     ));
    }
}
