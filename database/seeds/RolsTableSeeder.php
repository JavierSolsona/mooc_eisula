<?php

use Illuminate\Database\Seeder;

class RolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('rols')->insert(
        array(
          array(
            'name' => 'administrator',
          ),
          array(
            'name' => 'teacher',
          ),
          array(
            'name' => 'student',
          ),
      ));
    }
}
