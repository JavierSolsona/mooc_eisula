<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('types')->insert(
        array(
          array(
            'name' => 'VÍDEO',
          ),
          array(
            'name' => 'LINK',
          ),
          array(
            'name' => 'ARCHIVO',
          ),
          array(
            'name' => 'ASIGNACIÓN',
          ),
        ));
    }
}
