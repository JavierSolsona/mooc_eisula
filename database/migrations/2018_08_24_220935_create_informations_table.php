<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informations', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->integer('theme_id')->unsigned();
          $table->foreign('theme_id')->references('id')->on('themes');
          $table->string('route');
          $table->integer('type_id')->unsigned();
          $table->foreign('type_id')->references('id')->on('types');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informations');
    }
}
