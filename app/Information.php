<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Information extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'theme_id', 'route', 'type_id',
  ];

  protected $table = 'informations';

  static public function getInformation($theme_id) {
    return DB::table('informations')
              ->join('themes', 'themes.id', '=', 'informations.theme_id')
              ->join('types', 'types.id', '=', 'informations.type_id')
              ->where('themes.id', '=', $theme_id)
              ->select('informations.*', 'themes.name as theme_name', 'types.name as type_name')
              ->get();
  }

  static public function getTypes() {
    return DB::table('types')->select('name', 'id')->pluck('name', 'id');
  }

  static public function getVideos($theme_id, $user_id){
    return DB::table('informations')
              ->join('types', 'types.id', '=', 'informations.type_id')
              ->where('informations.theme_id', '?')
              ->where('types.name', '=', '?')
              ->select('informations.*', 'types.name as type_name',
                DB::raw('
                       (SELECT COUNT(*)
                       FROM information_views
                       WHERE information_views.user_id = ? AND information_views.information_id = informations.id
                       ) as view'
                     )
              )->setBindings([$user_id, $theme_id, 'VÍDEO'])->get();
  }

  static public function getLinks($theme_id){
    return DB::table('informations')
              ->join('types', 'types.id', '=', 'informations.type_id')
              ->where('informations.theme_id',$theme_id)
              ->where('types.name', '=', 'LINK')
              ->select('informations.*', 'types.name as type_name')
              ->get();
  }

  static public function getFiles($theme_id, $user_id){
    return DB::table('informations')
              ->join('types', 'types.id', '=', 'informations.type_id')
              ->where('informations.theme_id', '?')
              ->where('types.name', '=', '?')
              ->select('informations.*', 'types.name as type_name',
                DB::raw('
                     (SELECT COUNT(*)
                     FROM information_views
                     WHERE information_views.user_id = ? AND information_views.information_id = informations.id
                     ) as view'
                   )
            )->setBindings([$user_id, $theme_id, 'ARCHIVO'])->get();
  }

  static public function getAssignments($theme_id, $user_id){
    return DB::table('informations')
              ->join('types', 'types.id', '=', 'informations.type_id')
              ->where('informations.theme_id', '?')
              ->where('types.name', '=', '?')
              ->select('informations.*', 'types.name as type_name',
                 DB::raw('
                   (SELECT COUNT(*)
                   FROM information_views
                   WHERE information_views.user_id = ? AND information_views.information_id = informations.id
                   ) as view'
                 )
          )->setBindings([$user_id, $theme_id, 'ASIGNACIÓN'])->get();
  }

  static public function savedView($user_id, $information_id){
    try {
      DB::table('information_views')->insert(
          ['user_id' => $user_id, 'information_id' => $information_id]
      );
    } catch (\Exception $e) {
      return 0;
    }

    return 1;
  }

  static public function deleteInformationView($information_id) {
    DB::table('information_views')->where('information_id', '=', $information_id)->delete();
    return DB::table('information_views')->where('information_id', '=', $information_id)->count();
  }

  static public function deleteInformationViewUser($user_id) {
    DB::table('information_views')->where('user_id', '=', $user_id)->delete();
    return DB::table('information_views')->where('user_id', '=', $user_id)->count();
  }
}
