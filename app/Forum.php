<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Forum extends Model
{
  protected $fillable = [
      'theme_id',
  ];

  protected $table = 'forums';

  static public function getCourseOwner($theme_id) {
    return DB::table('themes')
              ->join('courses', 'courses.id', '=', 'themes.course_id')
              ->where('themes.id', '=', $theme_id)
              ->select('courses.user_id')
              ->first();
  }

  static public function getCommentsForCourse($course_id) {
    return DB::table('comments')
              ->join('users', 'users.id', '=', 'comments.user_id')
              ->join('forums', 'forums.id', '=', 'comments.forum_id')
              ->join('themes', 'themes.id', '=', 'forums.theme_id')
              ->join('courses', 'courses.id', '=', 'themes.course_id')
              ->where('courses.id', '=', $course_id)
              ->select('comments.*', 'users.name as user_name',
                       'themes.name as theme_name', 'courses.user_id as course_owner')
              ->orderBy('comments.id', 'asc')
              ->get();
  }
}
