<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCourse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|string|max:255|regex:/^[\pL\s0-9]+$/u',
          'description' => 'nullable|string',
          'course_id' => 'required',
          'image' => 'nullable|file|image|max:3072',
        ];
    }
}
