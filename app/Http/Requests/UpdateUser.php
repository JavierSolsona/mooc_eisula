<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Crypt;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|string|max:255|regex:/^[\pL\s]+$/u',
          'email' => 'required|string|email|max:255|unique:users,email,'.Crypt::decrypt(Request::input('id')),
          'password' => 'nullable|string|min:6|confirmed',
          'country_id' => 'required|integer|exists:countries,id',
          'photo' => 'nullable|file|image|max:3072',
        ];
    }
}
