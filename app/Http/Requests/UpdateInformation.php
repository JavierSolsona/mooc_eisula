<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class UpdateInformation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
           'name' => 'required|string|max:255|regex:/^[\pL\s0-9]+$/u',
           'type_id' => 'required|integer|exists:types,id',
           'user_id' => 'required',
           'information_id' => 'required',
         ];
     }

     public function validator(Factory $factory)
     {
         $validator = $factory->make($this->input(), $this->rules());
         $validator->sometimes('file', 'nullable|file', function($input) {
             return $input->type_id != 2;
         });

         $validator->sometimes('link', 'required|url', function($input) {
             return $input->type_id == 2;
         });
         return $validator;
     }
}
