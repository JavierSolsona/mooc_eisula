<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\Theme;
use App\Information;
use App\Forum;
use App\Comment;
use App\SignedUp;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Auth;
use Input;
use Storage;
use App\Http\Requests\StoreCourse;
use App\Http\Requests\UpdateCourse;
use App\Http\Requests\SearchCourse;

class CourseController extends Controller
{
    public function index($name, $id)  {
      Auth::user()->authorizeRoles(['teacher']);
      $id = base64_decode($id);

      if ($id != Auth::user()->id) {
        abort(404,'Página no encontrada.');
      }

      $courses = Course::where('user_id', $id)->get();

      return view('courses.index', compact('courses'));
    }

    public function create($name, $id)  {
      Auth::user()->authorizeRoles(['teacher']);
      $id = base64_decode($id);

      if ($id != Auth::user()->id) {
        abort(404,'Página no encontrada.');
      }

      return view('courses.create');
    }

    public function store(StoreCourse $request) {
      Auth::user()->authorizeRoles(['teacher']);

      try {
  			$user_id = Crypt::decrypt($request->user_id);
  		} catch (DecryptException $e) {
  			abort(404,'Página no encontrada.');
  		}


      if ($user_id != Auth::user()->id) {
        abort(404,'Página no encontrada.');
      }

      DB::beginTransaction();

      $course = new Course;

      $course->name = mb_strtoupper($request->name, 'UTF-8');
      $course->user_id = $user_id;
      $course->description = mb_strtoupper($request->description, 'UTF-8');
      $course->image_route = '';

      $saved = $course->save();

      if (!$saved) {
        DB::rollBack();
        return redirect()->route('course.create', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error Creando Curso');
      }

      if (Input::hasFile('image')) {

        if (Input::file('image')->isValid()) {

            $filename = $course->id .'/'.$request->image->getClientOriginalName();

            $saved = Storage::disk('courses')->put($filename, File::get($request->image));

            $course->image_route = 'courses/' . $filename;

            $saved2 = $course->save();

            if ((!$saved) || (!$saved2)) {
              DB::rollBack();
              return redirect()->route('course.create', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error Creando Curso');
            }

        }else{
          DB::rollBack();
          return redirect()->route('course.create', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error Imagen no valida');
        }

      }else {
        DB::rollBack();
        return redirect()->route('course.create', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error sin Imagen');
      }

      DB::commit();

      return redirect()->route('course.index', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('message', 'Curso Creado');
    }

    public function edit($name, $course_id)  {
      Auth::user()->authorizeRoles(['teacher']);
      $course_id = base64_decode($course_id);

      if (!is_numeric($course_id)) {
        abort(404,'Página no encontrada.');
      }

      $course = Course::find($course_id);

      if ((!isset($course->id)) || ($course->user_id != Auth::user()->id)) {
        abort(404,'Página no encontrada.');
      }

      return view('courses.edit', compact('course'));
    }

    public function update(UpdateCourse $request) {
      Auth::user()->authorizeRoles(['teacher']);

      try {
        $course_id = Crypt::decrypt($request->course_id);
      } catch (DecryptException $e) {
        abort(404,'Página no encontrada.');
      }

     if (!is_numeric($course_id)) {
        abort(404,'Página no encontrada.');
      }

      DB::beginTransaction();

      $course = Course::find($course_id);

      if ((!isset($course->id)) || ($course->user_id != Auth::user()->id)) {
        abort(404,'Página no encontrada.');
      }

       $course->name = mb_strtoupper($request->name, 'UTF-8');
       $course->description = mb_strtoupper($request->description, 'UTF-8');
       $saved = $course->save();

       if (!$saved) {
         DB::rollBack();
         return redirect()->route('course.edit', [str_replace(' ', '_', Auth::user()->name),  base64_encode($course->id)])->with('error_message', 'Error Editando Curso');
       }

       if (Input::hasFile('image')) {

         if (Input::file('image')->isValid()) {

           $deleted = Storage::disk('courses')->delete(substr($course->image_route, strpos($course->image_route, '/'), strlen($course->image_route)));

           $filename = $course->id .'/'.$request->image->getClientOriginalName();

           $saved = Storage::disk('courses')->put($filename, File::get($request->image));

           $course->image_route = 'courses/' . $filename;

           $saved2 = $course->save();

           if ((!$deleted) || (!$saved) || (!$saved2)){
             DB::rollBack();
             return redirect()->route('course.edit', [str_replace(' ', '_', Auth::user()->name),  base64_encode($course->id)])->with('error_message', 'Error Editando Curso');
           }

         }else{
           DB::rollBack();
           return redirect()->route('course.edit', [str_replace(' ', '_', Auth::user()->name),  base64_encode($course->id)])->with('error_message', 'Error Imagen no valida');
         }

       }

       DB::commit();

       return redirect()->route('course.index', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('message', 'Curso Editado');
    }

    public function delete($name, $course_id)  {
      Auth::user()->authorizeRoles(['teacher']);
      $course_id = base64_decode($course_id);

      if (!is_numeric($course_id)) {
        abort(404,'Página no encontrada.');
      }

      $course = Course::find($course_id);

      if ((!isset($course->id)) || ($course->user_id != Auth::user()->id)) {
        abort(404,'Página no encontrada.');
      }

       DB::beginTransaction();

       $signed_ups = SignedUp::where('course_id', $course->id)->get();

       $themes = Theme::where('course_id', $course->id)->get();

       try {
         foreach ($signed_ups as $signed_up) {
           $signed_up->delete();
         }

         foreach ($themes as $theme) {

           $informations =  Information::where('theme_id', $theme->id)->get();

           $forums =  Forum::where('theme_id', $theme->id)->get();

           foreach ($forums as $forum) {
             $comments = Comment::where('forum_id', $forum->id)->get();

             foreach ($comments as $comment) {
               $comment->delete();
             }

             $forum->delete();
           }

           foreach ($informations as $information) {

            $d = Information::deleteInformationView($information->id);

            if ($d != 0) {
              DB::rollback();
              return redirect()->route('course.index', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error Eliminando Curso');
            }

            $information->delete();
           }

           $theme->delete();
         }

         $course->delete();
         Storage::disk('courses')->deleteDirectory($course_id);

       } catch (\Exception $e) {
         DB::rollback();
         return redirect()->route('course.index', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error Eliminando Curso');
       }

       DB::commit();

       return redirect()->route('course.index', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('message', 'Curso Eliminado');
    }

    public function showAll() {
      if (Auth::check()) {
        $courses = Course::getCoursesWithAuth(Auth::id());
      }else{
        $courses = Course::getCoursesWithoutAuth();
      }

      return view('courses.showAll', compact('courses'));
    }

    public function search(SearchCourse $request) {
      $search = mb_strtoupper($request->search, 'UTF-8');

      if (Auth::check()) {
        $courses = Course::searchWithAuth($search, Auth::id());
      }else{
        $courses = Course::searchWithoutAuth($search);
      }

      return view('courses.search', compact('courses', 'search'));
    }
}
