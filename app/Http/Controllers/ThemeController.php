<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\Theme;
use App\Forum;
use App\Information;
use App\Comment;
use App\SignedUp;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Auth;
use Input;
use Storage;
use App\Http\Requests\StoreTheme;
use App\Http\Requests\UpdateTheme;

class ThemeController extends Controller
{
  public function index($course_name, $user_name, $user_id, $course_id)  {
    Auth::user()->authorizeRoles(['teacher']);
    $user_id = base64_decode($user_id);
    $course_id = base64_decode($course_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($course_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Course::where('id', $course_id)->where('user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

    $themes = Theme::getThemes($course_id);
    $course = Course::find($course_id);

    //$themes = Theme::where('course_id', $course_id)->get();

    return view('themes.index', compact('themes', 'course'));
  }

  public function create($course_name, $user_name, $user_id, $course_id)  {
    Auth::user()->authorizeRoles(['teacher']);
    $user_id = base64_decode($user_id);
    $course_id = base64_decode($course_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($course_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Course::where('id', $course_id)->where('user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     $course = Course::find($course_id);

    return view('themes.create', compact('course'));
  }

  public function store(StoreTheme $request) {
    Auth::user()->authorizeRoles(['teacher']);

    try {
      $user_id = Crypt::decrypt($request->user_id);
      $course_id = Crypt::decrypt($request->course_id);
    } catch (DecryptException $e) {
      abort(404,'Página no encontrada.');
    }

    if (($user_id != Auth::user()->id) ||  (!is_numeric($course_id))){
      abort(404,'Página no encontrada.');
    }

    $is_owner = Course::where('id', $course_id)->where('user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     DB::beginTransaction();

    $theme = new Theme;

    $theme->name = mb_strtoupper($request->name, 'UTF-8');
    $theme->course_id = $course_id;
    $theme->description = mb_strtoupper($request->description, 'UTF-8');

    $saved = $theme->save();

    $course = Course::find($course_id);

    if (!$saved) {
      DB::rollBack();
      return redirect()->route('theme.create', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])->with('error_message', 'Error Creando Tema');
    }

    $forum = new Forum;

    $forum->theme_id = $theme->id;

    $saved = $forum->save();

    if (!$saved) {
      DB::rollBack();
      return redirect()->route('theme.create', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])->with('error_message', 'Error Creando Tema');
    }

    DB::commit();

    return redirect()->route('theme.index', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])->with('message', 'Tema Creado');
  }

  public function edit($course_name, $user_name, $user_id, $theme_id)  {
    Auth::user()->authorizeRoles(['teacher']);
    $user_id = base64_decode($user_id);
    $theme_id = base64_decode($theme_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($theme_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Theme::join('courses', 'courses.id', '=', 'themes.course_id')->where('themes.id', $theme_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     $theme = Theme::find($theme_id);

    return view('themes.edit', compact('theme'));
  }

  public function update(UpdateTheme $request) {
    Auth::user()->authorizeRoles(['teacher']);

    try {
      $user_id = Crypt::decrypt($request->user_id);
      $theme_id = Crypt::decrypt($request->theme_id);
    } catch (DecryptException $e) {
      abort(404,'Página no encontrada.');
    }

   if (($user_id != Auth::user()->id) ||  (!is_numeric($theme_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Theme::join('courses', 'courses.id', '=', 'themes.course_id')->where('themes.id', $theme_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     $theme = Theme::find($theme_id);
     $course = Course::find($theme->course_id);

     $theme->name = mb_strtoupper($request->name, 'UTF-8');
     $theme->description = mb_strtoupper($request->description, 'UTF-8');
     $saved = $theme->save();

     if (!$saved) {
       return redirect()->route('theme.edit', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($theme->id)])->with('error_message', 'Error Editando Tema');
     }

     return redirect()->route('theme.index', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])->with('message', 'Tema Editado');
  }

  public function delete($course_name, $user_name, $user_id, $theme_id)  {
    Auth::user()->authorizeRoles(['teacher']);
    $user_id = base64_decode($user_id);
    $theme_id = base64_decode($theme_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($theme_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Theme::join('courses', 'courses.id', '=', 'themes.course_id')->where('themes.id', $theme_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     DB::beginTransaction();

     $informations =  Information::where('theme_id',$theme_id )->get();

     $forums =  Forum::where('theme_id',$theme_id )->get();

     $theme = Theme::find($theme_id);

     $course = Course::find($theme->course_id);

     try {
       foreach ($forums as $forum) {
         $comments = Comment::where('forum_id', $forum->id)->get();

         foreach ($comments as $comment) {
           $comment->delete();
         }

         $forum->delete();
       }

       foreach ($informations as $information) {
         $d = Information::deleteInformationView($information->id);

         if ($d != 0) {
           DB::rollback();
           return redirect()->route('theme.index', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])->with('error_message', 'Error Eliminando Tema');
         }

        $information->delete();
       }

       $theme->delete();
       Storage::disk('courses')->deleteDirectory($course->id.'/'.$theme_id);

     } catch (\Exception $e) {
       DB::rollback();
       return redirect()->route('theme.index', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])->with('error_message', 'Error Eliminando Tema');
     }

     DB::commit();

     return redirect()->route('theme.index', [str_replace(' ', '_', $course->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($course->id)])->with('message', 'Tema Eliminado');
  }

  public function show($course_name, $course_id)  {
    $course_id = base64_decode($course_id);

   if (!is_numeric($course_id)) {
      abort(404,'Página no encontrada.');
    }

    if (Auth::check()) {
      $themes = Theme::getThemesWithAuth($course_id, Auth::id());
      $course = Course::getCourseWithAuth($course_id, Auth::id());
    }else{
      $themes = Theme::getThemes($course_id);
      $course = Course::find($course_id);
    }

    return view('themes.show', compact('themes', 'course'));
  }
}
