<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\Theme;
use App\Information;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Auth;
use Input;
use Storage;
use App\Http\Requests\StoreInformation;
use App\Http\Requests\UpdateInformation;

class InformationController extends Controller
{
  public function index($theme_name, $user_name, $user_id, $theme_id)  {
    Auth::user()->authorizeRoles(['teacher']);
    $user_id = base64_decode($user_id);
    $theme_id = base64_decode($theme_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($theme_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Theme::join('courses', 'courses.id', '=', 'themes.course_id')->where('themes.id', $theme_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

    $informations = Information::getInformation($theme_id);
    $theme = Theme::find($theme_id);

    //$informations = Information::where('theme_id', $theme_id)->get();

    return view('informations.index', compact('informations', 'theme'));
  }

  public function create($theme_name, $user_name, $user_id, $theme_id)  {
    Auth::user()->authorizeRoles(['teacher']);
    $user_id = base64_decode($user_id);
    $theme_id = base64_decode($theme_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($theme_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Theme::join('courses', 'courses.id', '=', 'themes.course_id')->where('themes.id', $theme_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     $theme = Theme::find($theme_id);
     $type = Information::getTypes();

    return view('informations.create', compact('theme', 'type'));
  }

  public function store(StoreInformation $request) {
    Auth::user()->authorizeRoles(['teacher']);

    try {
      $user_id = Crypt::decrypt($request->user_id);
      $theme_id = Crypt::decrypt($request->theme_id);
    } catch (DecryptException $e) {
      abort(404,'Página no encontrada.');
    }

    if (($user_id != Auth::user()->id) ||  (!is_numeric($theme_id))){
      abort(404,'Página no encontrada.');
    }

    $is_owner = Theme::join('courses', 'courses.id', '=', 'themes.course_id')->where('themes.id', $theme_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     $theme = Theme::find($theme_id);

    DB::beginTransaction();

    $information = new Information;

    $information->name = mb_strtoupper($request->name, 'UTF-8');
    $information->theme_id = $theme_id;
    $information->type_id = $request->type_id;
    $information->route = '';
    $saved = $information->save();

    if (!$saved) {
      DB::rollBack();
      return redirect()->route('information.create', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($theme->id)])->with('error_message', 'Error Creando Recurso');
    }

    $information1 = Information::find($information->id);

    if($request->type_id == "2"){
      $information1->route = $request->link;

      $saved = $information1->save();
    }else {
      if (Input::hasFile('file')) {

        if (Input::file('file')->isValid()) {
          if ($request->file->getSize() <= 10485760) {
            $filename = $theme->course_id .'/'.$theme->id.'/'.$information1->id.'_'.$request->file->getClientOriginalName();

            Storage::disk('courses')->put($filename, File::get($request->file));

            $information1->route = 'courses/' . $filename;

            $saved = $information1->save();
          }else{
            DB::rollBack();
            return redirect()->route('information.create', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($theme->id)])->with('error_message', 'Error Archivo mayor de 10MB');
          }

        }else{
          DB::rollBack();
          return redirect()->route('information.create', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($theme->id)])->with('error_message', 'Error Archivo no valido');
        }

      }else {
        DB::rollBack();
        return redirect()->route('information.create', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($theme->id)])->with('error_message', 'Error sin Archivo');
      }
    }

    if (!$saved) {
      DB::rollBack();
      return redirect()->route('information.create', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($theme->id)])->with('error_message', 'Error Creando Recurso');
    }

    DB::commit();

    return redirect()->route('information.index', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])->with('message', 'Recurso Creado');
  }

  public function edit($theme_name, $user_name, $user_id, $information_id)  {
    Auth::user()->authorizeRoles(['teacher']);

    $user_id = base64_decode($user_id);
    $information_id = base64_decode($information_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($information_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Information::join('themes', 'themes.id', '=', 'informations.theme_id')->join('courses', 'courses.id', '=', 'themes.course_id')->where('informations.id', $information_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     $information = Information::find($information_id);
     $type = Information::getTypes();

    return view('informations.edit', compact('information', 'type'));
  }

  public function update(UpdateInformation $request) {
    Auth::user()->authorizeRoles(['teacher']);

    try {
      $user_id = Crypt::decrypt($request->user_id);
      $information_id = Crypt::decrypt($request->information_id);
    } catch (DecryptException $e) {
      abort(404,'Página no encontrada.');
    }

    if (($user_id != Auth::user()->id) ||  (!is_numeric($information_id))){
      abort(404,'Página no encontrada.');
    }

    $is_owner = Information::join('themes', 'themes.id', '=', 'informations.theme_id')->join('courses', 'courses.id', '=', 'themes.course_id')->where('informations.id', $information_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

    DB::beginTransaction();

    $information = Information::find($information_id);
    $theme = Theme::find($information->theme_id);

    $previus_type = $information->type_id;

    $information->name = mb_strtoupper($request->name, 'UTF-8');
    $information->type_id = $request->type_id;
    $saved = $information->save();

    if (!$saved) {
      DB::rollBack();
      return redirect()->route('information.edit', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($information->id)])->with('error_message', 'Error Editando Recurso');
    }

    if($request->type_id == "2"){

      if ($previus_type != "2") {
        Storage::disk('courses')->delete(substr($information->route, strpos($information->route, '/'), strlen($information->route)));
      }

      $information->route = $request->link;

      $saved = $information->save();

      if (!$saved) {
        DB::rollBack();
        return redirect()->route('information.edit', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($information->id)])->with('error_message', 'Error Editando Recurso');
      }

    }else {
      if (Input::hasFile('file')) {

        if (Input::file('file')->isValid()) {

          if ($request->file->getSize() <= 10485760) {
            if ($previus_type != "2") {
              Storage::disk('courses')->delete(substr($information->route, strpos($information->route, '/'), strlen($information->route)));
            }

            $filename = $theme->course_id .'/'.$theme->id.'/'.$information->id.'_'.$request->file->getClientOriginalName();

            Storage::disk('courses')->put($filename, File::get($request->file));

            $information->route = 'courses/' . $filename;

            $saved = $information->save();

            if (!$saved) {
              DB::rollBack();
              return redirect()->route('information.edit', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($information->id)])->with('error_message', 'Error Editando Recurso');
            }

          }else {
            DB::rollBack();
            return redirect()->route('information.edit', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($information->id)])->with('error_message', 'Error Archivo mayor de 10MB');
          }
        }else{
          DB::rollBack();
          return redirect()->route('information.edit', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id),  base64_encode($information->id)])->with('error_message', 'Error Archivo no valido');
        }

      }
    }

    DB::commit();

    return redirect()->route('information.index', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])->with('message', 'Recurso Editado');
  }

  public function delete($theme_name, $user_name, $user_id, $information_id)  {
    Auth::user()->authorizeRoles(['teacher']);

    $user_id = base64_decode($user_id);
    $information_id = base64_decode($information_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($information_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Information::join('themes', 'themes.id', '=', 'informations.theme_id')->join('courses', 'courses.id', '=', 'themes.course_id')->where('informations.id', $information_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0) {
       abort(404,'Página no encontrada.');
     }

     DB::beginTransaction();

     try {
       $information = Information::find($information_id);
       $theme = Theme::find($information->theme_id);

       $d = Information::deleteInformationView($information->id);

       if ($d != 0) {
         DB::rollback();
         return redirect()->route('information.index', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])->with('error_message', 'Error Eliminando Recurso');
       }

       if ($information->type_id != "2") {
         Storage::disk('courses')->delete(substr($information->route, strpos($information->route, '/'), strlen($information->route)));
       }

      $information->delete();
     } catch (\Exception $e) {
        DB::rollback();
        return redirect()->route('information.index', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])->with('error_message', 'Error Eliminando Recurso');
     }

     DB::commit();

    return redirect()->route('information.index', [str_replace(' ', '_', $theme->name), str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id), base64_encode($theme->id)])->with('message', 'Recurso Eliminado');

  }

  public function show($theme_name, $user_id, $theme_id)  {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);
    $user_id = base64_decode($user_id);
    $theme_id = base64_decode($theme_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($theme_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_signed = Theme::join('courses', 'courses.id',  '=', 'themes.course_id')->join('signed_ups', 'signed_ups.course_id',  '=', 'courses.id')->where('themes.id', $theme_id)->where('signed_ups.user_id', $user_id)->count();

    if ($is_signed == 0) {
       abort(401,'No esta inscrito.');
     }

    $data = Theme::getData($theme_id);

    $videos = Information::getVideos($theme_id, $user_id);

    $files = Information::getFiles($theme_id, $user_id);

    $links = Information::getLinks($theme_id);

    $assignments = Information::getAssignments($theme_id, $user_id);

    return view('informations.show', compact('videos', 'files', 'links', 'assignments', 'data'));
  }

  public function showVideo($user_id, $information_id)  {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);
    $user_id = base64_decode($user_id);
    $information_id = base64_decode($information_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($information_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_signed = Information::join('themes', 'themes.id',  '=', 'informations.theme_id')->join('courses', 'courses.id',  '=', 'themes.course_id')->join('signed_ups', 'signed_ups.course_id',  '=', 'courses.id')->where('informations.id', $information_id)->where('signed_ups.user_id', $user_id)->count();

    $is_owner =  Information::join('themes', 'themes.id',  '=', 'informations.theme_id')->join('courses', 'courses.id', '=', 'themes.course_id')->where('informations.id', $information_id)->where('courses.user_id', $user_id)->count();

    if (($is_signed == 0) && ($is_owner == 0)) {
       abort(401,'No esta inscrito.');
     }

    $video = Information::find($information_id);

    return view('informations.showVideo', compact('video'));
  }

  public function informationView(Request $request) {
    $user_id = base64_decode($request->user_id);
    $information_id = base64_decode($request->information_id);

    if ((!is_numeric($user_id)) ||  (!is_numeric($information_id))) {
      $ret = ['estado' => 'error'];
      return $ret;
     }

     $saved = Information::savedView($user_id, $information_id);

     if ($saved) {
       $ret = ['estado' => 'ok'];
     }else {
       $ret = ['estado' => 'error'];
     }

     return $ret;
  }
}
