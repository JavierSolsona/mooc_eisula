<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use File;
use Input;
use Storage;
use DB;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255|regex:/^[\pL\s]+$/u',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'country_id' => 'required|integer|exists:countries,id',
            'photo' => 'nullable|file|image|max:3072',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        DB::beginTransaction();

        $user = User::create([
            'name' => mb_strtoupper($data['name'], 'UTF-8'),
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'rol_id' => '3',
            'country_id' => $data['country_id'],
            'universityStudent' => isset($data['universityStudent']) ? '1' : '0',
        ]);

        if (!$user->id) {
          DB::rollBack();
          $user = new User();
          $user->name = "-1";
          return $user;
        }

        $user1 = User::find($user->id);

        if (Input::hasFile('photo')) {

          if (Input::file('photo')->isValid()) {

            $filename = $user1->id.'_'.$data['photo']->getClientOriginalName();

            Storage::disk('profile_pictures')->put($filename, File::get($data['photo']));

            $user1->photo = 'profile_pictures/' . $filename;

            $saved = $user1->save();

          }else{
            $user1->photo = 'profile_pictures/user.png';

            $saved = $user1->save();
          }

        }else {
          $user1->photo = 'profile_pictures/user.png';

          $saved = $user1->save();
        }

        if (!$saved) {
          DB::rollBack();
          $user = new User();
          $user->name = "-1";
          return $user;
        }


        DB::commit();

        return $user;
    }

    /**
    * Show the application registration form.
    *
    * @return \Illuminate\Http\Response
    */
    public function showRegistrationForm()
    {
        $country = User::getConuntries();

        return view('auth.register', compact('country'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        if ($user->name == "-1") {
          return redirect()->route('register')->with('error_message', 'Error Creando Usuario');
        }

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath())->with('message', 'Usuario Creado');
    }

}
