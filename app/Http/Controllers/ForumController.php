<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\Theme;
use App\Forum;
use App\Comment;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Auth;
use Input;
use Storage;

class ForumController extends Controller
{
  public function index($theme_name, $user_id, $theme_id)  {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);
    $user_id = base64_decode($user_id);
    $theme_id = base64_decode($theme_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($theme_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_signed = Theme::join('courses', 'courses.id',  '=', 'themes.course_id')->join('signed_ups', 'signed_ups.course_id',  '=', 'courses.id')->where('themes.id', $theme_id)->where('signed_ups.user_id', $user_id)->count();

    $is_owner = Theme::join('courses', 'courses.id', '=', 'themes.course_id')->where('themes.id', $theme_id)->where('courses.user_id', $user_id)->count();

    if (($is_signed == 0) && ($is_owner == 0)){
       abort(401,'No esta inscrito.');
     }

     if ($is_owner > 0) {
       $owner = 1;
     }else {
       $owner = 0;
     }

     $course = Forum::getCourseOwner($theme_id);

    $forum = Forum::where('theme_id', $theme_id)->first();

    $comments = Comment::getComments($forum->id);

    return view('forums.index', compact('forum', 'comments', 'owner', 'course'));
  }

  public function courseForum($course_name, $user_id, $course_id)  {
    Auth::user()->authorizeRoles(['teacher']);
    $user_id = base64_decode($user_id);
    $course_id = base64_decode($course_id);

   if (($user_id != Auth::user()->id) ||  (!is_numeric($course_id))) {
      abort(404,'Página no encontrada.');
    }

    $is_owner = Course::where('courses.id', $course_id)->where('courses.user_id', $user_id)->count();

    if ($is_owner == 0){
       abort(404,'Página no encontrada.');
     }

    $comments = Forum::getCommentsForCourse($course_id);

    return view('forums.courseForum', compact('comments'));
  }
}
