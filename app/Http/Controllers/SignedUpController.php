<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SignedUp;
use Illuminate\Contracts\Encryption\DecryptException;
use Auth;

class SignedUpController extends Controller
{
  public function signedUp(Request $request) {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);

    if ($request->user_id != Auth::user()->id) {
      abort(404,'Página no encontrada.');
    }

    $ret = [];

    $signedUp = new SignedUp;

    $signedUp->user_id = $request->user_id;
    $signedUp->course_id = $request->course_id;

    $saved = $signedUp->save();

    if (!$saved) {
      $ret = ['estado' => 'error'];
    }else{
      $ret = ['estado' => 'ok'];
    }
    return $ret;
  }

  public function remove(Request $request) {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);

    if ($request->user_id != Auth::user()->id) {
      abort(404,'Página no encontrada.');
    }

    $ret = [];

    $signed_ups = SignedUp::where('user_id', $request->user_id)->where('course_id',$request->course_id)->get();

    try {
      foreach ($signed_ups as $signed_up) {
        $signed_up->delete();
      }
    } catch (\Exception $e) {
      $ret = ['estado' => 'error'];
      return $ret;
    }

    $ret = ['estado' => 'ok'];

    return $ret;
  }

  public function signed($name, $id)  {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);
    $id = base64_decode($id);

    if ($id != Auth::user()->id) {
      abort(404,'Página no encontrada.');
    }

    $courses = SignedUp::getCourseSigned($id);

    return view('signed_up.signed', compact('courses'));
  }

}
