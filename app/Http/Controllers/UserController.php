<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\SignedUp;
use App\Comment;
use App\Theme;
use App\Information;
use App\Forum;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Auth;
use Input;
use Storage;
use Hash;
use App\Http\Requests\UpdateUser;
use App\Http\Requests\StoreTeacher;
use App\Http\Requests\RecoverBackup;

class UserController extends Controller
{
  public function edit($name, $id)  {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);
    $id = base64_decode($id);

    if ($id != Auth::user()->id) {
      abort(404,'Página no encontrada.');
    }

    $country = User::getConuntries();

    $user = User::find($id);

    return view('users.edit', compact('country', 'user'));
  }

  public function update(UpdateUser $request) {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);

    try {
			$id = Crypt::decrypt($request->id);
		} catch (DecryptException $e) {
			abort(404,'Página no encontrada.');
		}


    if ($id != Auth::user()->id) {
      abort(404,'Página no encontrada.');
    }

    DB::beginTransaction();

    $user = User::find($id);

    $user->name = mb_strtoupper($request->name, 'UTF-8');
    $user->email = $request->email;
    $user->country_id = $request->country_id;
    $user->universityStudent = isset($request->universityStudent) ? '1' : '0';
    if (strlen($request->password) > 0 ) {
      $user->password = Hash::make($request->password);
    }

    $user->save();

    if (!$user) {
      DB::rollBack();
      return redirect()->route('user.edit', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error Editando Usuario');
    }

    if (Input::hasFile('photo')) {

      if (Input::file('photo')->isValid()) {

        if ($user->photo != "profile_pictures/user.png") {
              Storage::disk('profile_pictures')->delete(substr($user->photo, strpos($user->photo, '/'), strlen($user->photo)));
        }

        $filename = $user->id.'_'.$request->photo->getClientOriginalName();

        Storage::disk('profile_pictures')->put($filename, File::get($request->photo));

        $route = 'profile_pictures/'.$filename;

        $user->photo = $route;

        $user->save();

        if (!$user) {
          DB::rollBack();
          return redirect()->route('user.edit', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error Editando Usuario');
        }

      }else{
        DB::rollBack();
        return redirect()->route('user.edit', [str_replace(' ', '_', Auth::user()->name), base64_encode(Auth::user()->id)])->with('error_message', 'Error Editando Usuario');
      }

    }

    DB::commit();

    return redirect()->route('/')->with('message', 'Éxito Editando Usuario');
  }

  public function createTeacher(){
    Auth::user()->authorizeRoles(['administrator']);

    $country = User::getConuntries();

    return view('users.createTeacher', compact('country'));
  }

  public function storeTeacher(StoreTeacher $request) {
    Auth::user()->authorizeRoles(['administrator']);

    DB::beginTransaction();

    $user = new User;

    $user->name = mb_strtoupper($request->name, 'UTF-8');
    $user->email = $request->email;
    $user->password = Hash::make($request->password);
    $user->rol_id = '2';
    $user->country_id = $request->country_id;
    $user->universityStudent = isset($request->universityStudent) ? '1' : '0';

    $user->save();

    if (!$user) {
      DB::rollBack();
      return redirect()->route('teacher.create')->with('error_message', 'Error Creando Profesor');
    }

    $user1 = User::find($user->id);

    if (Input::hasFile('photo')) {

      if (Input::file('photo')->isValid()) {

        $filename = $user1->id.'_'.$request->photo->getClientOriginalName();

        Storage::disk('profile_pictures')->put($filename, File::get($request->photo));

        $user1->photo = 'profile_pictures/' . $filename;

        $user1->save();

      }else{
        $user1->photo = 'profile_pictures/user.png';

        $user1->save();
      }

    }else {
      $user1->photo = 'profile_pictures/user.png';

      $user1->save();
    }

    if (!$user1) {
      DB::rollBack();
      return redirect()->route('teacher.create')->with('error_message', 'Error Creando Profesor');
    }


    DB::commit();

    return redirect()->route('/')->with('message', 'Profesor Creado');

  }

  public function delete($name, $id)  {
    Auth::user()->authorizeRoles(['teacher', 'student']);
    $id = base64_decode($id);

    if ($id != Auth::user()->id) {
      abort(404,'Página no encontrada.');
    }

    DB::beginTransaction();

    $user = User::find($id);

    $user_signed_ups = SignedUp::where('user_id', $user->id)->get();

    $user_comments = Comment::where('user_id', $user->id)->get();

    try {
      foreach ($user_signed_ups as $user_signed_up) {
        $user_signed_up->delete();
      }

      foreach ($user_comments as $user_comment) {
        $user_comment->delete();
      }

      $d = Information::deleteInformationViewUser($user->id);

      if ($d != 0) {
        DB::rollback();
        return redirect()->route('/')->with('error_message', 'Error Eliminando Cuenta');
      }

      if ($user->rol_id == 2) {
        $courses = Course::where('user_id', $user->id)->get();

        foreach ($courses as $course) {
          $signed_ups = SignedUp::where('course_id', $course->id)->get();

          $themes = Theme::where('course_id', $course->id)->get();
          foreach ($signed_ups as $signed_up) {
            $signed_up->delete();
          }

          foreach ($themes as $theme) {

            $informations =  Information::where('theme_id', $theme->id)->get();

            $forums =  Forum::where('theme_id', $theme->id)->get();

            foreach ($forums as $forum) {
              $comments = Comment::where('forum_id', $forum->id)->get();

              foreach ($comments as $comment) {
                $comment->delete();
              }

              $forum->delete();
            }

            foreach ($informations as $information) {

              $d = Information::deleteInformationView($information->id);

              if ($d != 0) {
                DB::rollback();
                return redirect()->route('/')->with('error_message', 'Error Eliminando Cuenta');
              }

             $information->delete();
            }

            $theme->delete();
          }

          $course->delete();
          Storage::disk('courses')->deleteDirectory($course->id);
        }
      }

      Auth::guard()->logout();

      $user->delete();

    } catch (\Exception $e) {
      DB::rollback();
      return redirect()->route('/')->with('error_message', 'Error Eliminando Cuenta');
    }

    DB::commit();

    return redirect()->route('/')->with('message', 'Cuenta Eliminada');
  }

  public function createBackup()  {
    Auth::user()->authorizeRoles(['administrator']);

    exec("pg_dump -h " . env('DB_HOST', '127.0.0.1') ." -p " . env('DB_PORT', '5432') . " -U " . env('DB_USERNAME', 'forge') . " -w -b -F c -f  " . Storage::disk('backup')->getDriver()->getAdapter()->getPathPrefix() . "/backup.sql -d " . env('DB_DATABASE', 'forge'), $output, $ret);

    if ($ret != 0) {
      return redirect()->route('/')->with('error_message', 'Error en el respaldo');
    }

    $ret = File::copyDirectory(Storage::disk('courses')->getDriver()->getAdapter()->getPathPrefix(), Storage::disk('backup')->getDriver()->getAdapter()->getPathPrefix() . '/courses', true);

    if ($ret != 1) {
      return redirect()->route('/')->with('error_message', 'Error en el respaldo');
    }

    $ret = File::copyDirectory(Storage::disk('profile_pictures')->getDriver()->getAdapter()->getPathPrefix(), Storage::disk('backup')->getDriver()->getAdapter()->getPathPrefix() . '/profile_pictures', true);

    if ($ret != 1) {
      return redirect()->route('/')->with('error_message', 'Error en el respaldo');
    }

    $ret = File::copyDirectory(Storage::disk('public_path')->getDriver()->getAdapter()->getPathPrefix() . '/images', Storage::disk('backup')->getDriver()->getAdapter()->getPathPrefix() . '/images', true);

    if ($ret != 1) {
      return redirect()->route('/')->with('error_message', 'Error en el respaldo');
    }

    exec("tar -czf ". public_path() . "/backup.tar.gz -C " . Storage::disk('backup')->getDriver()->getAdapter()->getPathPrefix() . " .", $output, $ret);

    if ($ret != 0) {
      return redirect()->route('/')->with('error_message', 'Error en el respaldo');
    }

    Storage::disk('public_path')->deleteDirectory('backups');

    return Storage::disk('public_path')->download('backup.tar.gz');
  }

  public function recoverBackup(RecoverBackup $request) {
    Auth::user()->authorizeRoles(['administrator']);

    if (Input::hasFile('file')) {

      if (Input::file('file')->isValid()) {

          $filename = 'recover/' . $request->file->getClientOriginalName();

          $store = Storage::disk('public_path')->put($filename, File::get($request->file));

          if ($store != 1) {
            return redirect()->back()->with('error_message', 'Error intente nuevamente');
          }

          //Remove the existing files
          Storage::disk('public_path')->deleteDirectory('profile_pictures');
          Storage::disk('public_path')->deleteDirectory('courses');
          Storage::disk('public_path')->deleteDirectory('images');

          exec("tar -xzf ". public_path() . '/' .$filename . ' --overwrite', $output, $ret);

          if ($ret != 0) {
            return redirect()->back()->with('error_message', 'Error en la descompresión');
          }

          Storage::disk('public_path')->deleteDirectory('recover');

          exec("pg_restore -h " . env('DB_HOST', '127.0.0.1') ." -p " . env('DB_PORT', '5432') . " -U " . env('DB_USERNAME', 'forge') . " -w -c -F c --if-exists -d " . env('DB_DATABASE', 'forge') . " " . Storage::disk('public_path')->getDriver()->getAdapter()->getPathPrefix() . '/backup.sql' , $output, $ret);

          if ($ret != 0) {
            return redirect()->back()->with('error_message', 'Error en la carga de la Base de Datos');
          }

          Storage::disk('public_path')->delete('backup.sql');

      }else{
        return redirect()->back()->with('error_message', 'Error Archivo no valido');
      }

    }else {
      return redirect()->back()->with('error_message', 'Error sin Archivo');
    }

    return redirect()->back()->with('message', 'Respaldo Recuperado');
  }

  public function stats()  {
    Auth::user()->authorizeRoles(['administrator']);

    $students_number = User::studentsNumber();

    $teachers_number = User::teachersNumber();

    $courses_number = Course::coursesNumber();

    $signed_up_average = SignedUp::average($courses_number);

    $teachers_number_courses = Course::coursesNumberByTeacher();

    $courses_number_students = SignedUp::studentsByCourse();

    return view('users.stats', compact('students_number', 'teachers_number',
                                       'courses_number', 'signed_up_average',
                                       'teachers_number_courses', 'courses_number_students'));
  }

  public function manualIndex()  {
    return view('users.manualIndex');
  }

  public function manualShow($name)  {
    return view('users.manualShow', compact('name'));
  }
}
