<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Auth;
use Input;
use Storage;
use DB;
use File;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Requests\StoreSlider;
use App\Http\Requests\UpdateSlider;

class SliderController extends Controller
{
  public function index() {
    Auth::user()->authorizeRoles(['administrator']);
    $sliders = Slider::all();

    return view('sliders.index', compact('sliders'));
  }

  public function create() {
    Auth::user()->authorizeRoles(['administrator']);
    return view('sliders.create');
  }

  public function store(StoreSlider $request) {
		Auth::user()->authorizeRoles(['administrator']);
		DB::beginTransaction();

		$slider = new Slider;

		$slider->image_route = "";

		$saved = $slider->save();

		if (!$saved) {
			DB::rollBack();
			return redirect()->route('slider.create')->with('error_message', 'Error Creando Imagen');
		}

    if (Input::hasFile('image')) {

      if (Input::file('image')->isValid()) {

          $filename = $slider->id .'_'.$request->image->getClientOriginalName();

          $saved = Storage::disk('images')->put($filename, File::get($request->image));

          $slider->image_route = 'images/' . $filename;

          $saved2 = $slider->save();

          if ((!$saved) || (!$saved2)) {
            DB::rollBack();
            return redirect()->route('slider.create')->with('error_message', 'Error Creando Imagen');
          }

      }else{
        DB::rollBack();
        return redirect()->route('slider.create')->with('error_message', 'Error Imagen no valida');
      }

    }else {
      DB::rollBack();
      return redirect()->route('slider.create')->with('error_message', 'Error sin Imagen');
    }

		DB::commit();

		return redirect()->route('slider.index')->with('message', 'Imagen Creada Exitosamente.');

	}

  public function edit($slider_id)  {
    Auth::user()->authorizeRoles(['administrator']);
    $slider_id = base64_decode($slider_id);

    if (!is_numeric($slider_id)) {
      abort(404,'Página no encontrada.');
    }

    $slider = Slider::find($slider_id);

    if (!isset($slider->id)) {
      abort(404,'Página no encontrada.');
    }

    return view('sliders.edit', compact('slider'));
  }

  public function update(UpdateSlider $request) {
		Auth::user()->authorizeRoles(['administrator']);

    try {
      $slider_id = Crypt::decrypt($request->slider_id);
    } catch (DecryptException $e) {
      abort(404,'Página no encontrada.');
    }

   if (!is_numeric($slider_id)) {
      abort(404,'Página no encontrada.');
    }

    DB::beginTransaction();

		$slider = Slider::find($slider_id);

		if (Input::hasFile('image')) {
			if (Input::file('image')->isValid()) {

        $deleted = Storage::disk('images')->delete(substr($slider->image_route, strpos($slider->image_route, '/'), strlen($slider->image_route)));

        $filename = $slider->id .'_'.$request->image->getClientOriginalName();

        $saved = Storage::disk('images')->put($filename, File::get($request->image));

        $slider->image_route = 'images/' . $filename;

        $saved2 = $slider->save();

        if ((!$deleted) || (!$saved) || (!$saved2)){
          DB::rollBack();
          return redirect()->route('slider.edit', [base64_encode($slider->id)])->with('error_message', 'Error Editando Imagen');
        }
			}else{
        DB::rollBack();
        return redirect()->route('slider.edit', [base64_encode($slider->id)])->with('error_message', 'Error Imagen no valida');
      }

		}else {
      DB::rollBack();
      return redirect()->route('slider.edit', [base64_encode($slider->id)])->with('error_message', 'Error sin Imagen');
    }

    DB::commit();

		return redirect()->route('slider.index')->with('message', 'Imagen Editada.');
	}

  public function delete($slider_id)  {
    Auth::user()->authorizeRoles(['administrator']);
    $slider_id = base64_decode($slider_id);

    if (!is_numeric($slider_id)) {
      abort(404,'Página no encontrada.');
    }

    DB::beginTransaction();

    $slider = Slider::find($slider_id);

    $deleted = Storage::disk('images')->delete(substr($slider->image_route, strpos($slider->image_route, '/'), strlen($slider->image_route)));

    $deleted2 = $slider->delete();

    if ((!$deleted) || (!$deleted2)){
      DB::rollBack();
      return redirect()->route('slider.index')->with('error_message', 'Error Eliminando Imagen');
    }

     DB::commit();

     return redirect()->route('slider.index')->with('message', 'Imagen Eliminada');
  }
}
