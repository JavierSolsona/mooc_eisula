<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Forum;
use App\Comment;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Auth;

class CommentController extends Controller
{
  public function store(Request $request) {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);

    $ret = [];

    try {
      $user_id = Crypt::decrypt($request->user_id);
      $forum_id = Crypt::decrypt($request->forum_id);
    } catch (DecryptException $e) {
      $ret = ['estado' => 'error'];
      return $ret;
    }

    if (!is_numeric($user_id) || !is_numeric($forum_id) || ($user_id != Auth::user()->id)) {
      $ret = ['estado' => 'error'];
      return $ret;
     }

     $is_signed = Forum::join('themes', 'themes.id',  '=', 'forums.theme_id')->join('courses', 'courses.id',  '=', 'themes.course_id')->join('signed_ups', 'signed_ups.course_id',  '=', 'courses.id')->where('forums.id', $forum_id)->where('signed_ups.user_id', $user_id)->count();

     $is_owner = Forum::join('themes', 'themes.id',  '=', 'forums.theme_id')->join('courses', 'courses.id',  '=', 'themes.course_id')->where('forums.id', $forum_id)->where('courses.user_id', $user_id)->count();

     if (($is_signed == 0) && ($is_owner == 0)) {
       $ret = ['estado' => 'error'];
       return $ret;
      }

      $comment = new Comment;
      $comment->user_id = $user_id;
      $comment->forum_id = $forum_id;
      $comment->comment = $request->forum_comment_comment;
      $comment->edit = '0';

      $saved = $comment->save();

      if ($saved) {
        $comment = Comment::getComment($comment->id);
        $ret = ['estado' => 'ok', 'comment' => $comment];
      }else{
        $ret = ['estado' => 'error'];
      }

      return $ret;
  }

  public function update(Request $request) {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);

    $ret = [];

    $comment_id = base64_decode($request->comment_id_update);

   if (!is_numeric($comment_id)) {
     $ret = ['estado' => 'error'];
     return $ret;
    }

     $is_signed = Comment::join('forums', 'forums.id',  '=', 'comments.forum_id')->join('themes', 'themes.id',  '=', 'forums.theme_id')
                         ->join('courses', 'courses.id',  '=', 'themes.course_id')->join('signed_ups', 'signed_ups.course_id',  '=', 'courses.id')
                         ->where('comments.id', $comment_id)->where('signed_ups.user_id', Auth::user()->id)->count();

     $is_owner = Comment::join('forums', 'forums.id',  '=', 'comments.forum_id')->join('themes', 'themes.id',  '=', 'forums.theme_id')
                         ->join('courses', 'courses.id',  '=', 'themes.course_id')->where('comments.id', $comment_id)->where('courses.user_id', Auth::user()->id)->count();

     if (($is_signed == 0) && ($is_owner == 0)) {
       $ret = ['estado' => 'error'];
       return $ret;
      }

      $comment = Comment::find($comment_id);

      if (Auth::user()->id !=  $comment->user_id) {
        $ret = ['estado' => 'error'];
        return $ret;
      }

      $comment->comment = $request->forum_comment_comment_update;
      $comment->edit = '1';

      $saved = $comment->save();

      if ($saved) {
        $comment = Comment::getComment($comment->id);
        $ret = ['estado' => 'ok', 'comment' => $comment];
      }else{
        $ret = ['estado' => 'error'];
      }

      return $ret;
  }

  public function delete(Request $request) {
    Auth::user()->authorizeRoles(['administrator', 'teacher', 'student']);

    $ret = [];

    $comment_id = base64_decode($request->comment_id);

   if (!is_numeric($comment_id)) {
     $ret = ['estado' => 'error'];
     return $ret;
    }

     $is_signed = Comment::join('forums', 'forums.id',  '=', 'comments.forum_id')->join('themes', 'themes.id',  '=', 'forums.theme_id')
                         ->join('courses', 'courses.id',  '=', 'themes.course_id')->join('signed_ups', 'signed_ups.course_id',  '=', 'courses.id')
                         ->where('comments.id', $comment_id)->where('signed_ups.user_id', Auth::user()->id)->count();

     $is_owner = Comment::join('forums', 'forums.id',  '=', 'comments.forum_id')->join('themes', 'themes.id',  '=', 'forums.theme_id')
                         ->join('courses', 'courses.id',  '=', 'themes.course_id')->where('comments.id', $comment_id)->where('courses.user_id', Auth::user()->id)->count();

     if (($is_signed == 0) && ($is_owner == 0)) {
       $ret = ['estado' => 'error'];
       return $ret;
      }

      $comment = Comment::find($comment_id);

      if ((Auth::user()->id !=  $comment->user_id) && ($is_owner == 0)) {
        $ret = ['estado' => 'error'];
        return $ret;
      }

      $delete = $comment->delete();

      if ($delete) {
        $ret = ['estado' => 'ok', 'id' => $comment_id];
      }else{
        $ret = ['estado' => 'error'];
      }

      return $ret;
  }

  public function courseUpdate(Request $request) {
    Auth::user()->authorizeRoles(['teacher']);

    $ret = [];

    $comment_id = base64_decode($request->comment_id_update);

   if (!is_numeric($comment_id)) {
     $ret = ['estado' => 'error'];
     return $ret;
    }

     $is_owner = Comment::join('forums', 'forums.id',  '=', 'comments.forum_id')->join('themes', 'themes.id',  '=', 'forums.theme_id')
                         ->join('courses', 'courses.id',  '=', 'themes.course_id')->where('comments.id', $comment_id)->where('courses.user_id', Auth::user()->id)->count();

     if ($is_owner == 0) {
       $ret = ['estado' => 'error'];
       return $ret;
      }

      $comment = Comment::find($comment_id);

      if (Auth::user()->id !=  $comment->user_id) {
        $ret = ['estado' => 'error'];
        return $ret;
      }

      $comment->comment = $request->forum_comment_comment_update;
      $comment->edit = '1';

      $saved = $comment->save();

      if ($saved) {
        $comment = Comment::getCommentForCourse($comment->id);
        $ret = ['estado' => 'ok', 'comment' => $comment];
      }else{
        $ret = ['estado' => 'error'];
      }

      return $ret;
  }

  public function courseResponse(Request $request) {
    Auth::user()->authorizeRoles(['teacher']);

    $ret = [];

    $user_id = base64_decode($request->forum_comment_response_user_id);
    $forum_id = base64_decode($request->forum_comment_response_forum_id);

    if (!is_numeric($user_id) || !is_numeric($forum_id) || ($user_id != Auth::user()->id)) {
      $ret = ['estado' => 'error'];
      return $ret;
     }

     $is_owner = Forum::join('themes', 'themes.id',  '=', 'forums.theme_id')->join('courses', 'courses.id',  '=', 'themes.course_id')->where('forums.id', $forum_id)->where('courses.user_id', $user_id)->count();

     if ($is_owner == 0) {
       $ret = ['estado' => 'error'];
       return $ret;
      }

      $comment = new Comment;
      $comment->user_id = $user_id;
      $comment->forum_id = $forum_id;
      $comment->comment = $request->forum_comment_response_comment;
      $comment->edit = '0';

      $saved = $comment->save();

      if ($saved) {
        $comment = Comment::getCommentForCourse($comment->id);
        $ret = ['estado' => 'ok', 'comment' => $comment];
      }else{
        $ret = ['estado' => 'error'];
      }

      return $ret;
  }

  public function response(Request $request) {
    Auth::user()->authorizeRoles(['teacher']);

    $ret = [];

    try {
      $user_id = Crypt::decrypt($request->forum_comment_response_user_id);
      $forum_id = Crypt::decrypt($request->forum_comment_response_forum_id);
    } catch (DecryptException $e) {
      $ret = ['estado' => 'error'];
      return $ret;
    }

    if (!is_numeric($user_id) || !is_numeric($forum_id) || ($user_id != Auth::user()->id)) {
      $ret = ['estado' => 'error'];
      return $ret;
     }

     $is_owner = Forum::join('themes', 'themes.id',  '=', 'forums.theme_id')->join('courses', 'courses.id',  '=', 'themes.course_id')->where('forums.id', $forum_id)->where('courses.user_id', $user_id)->count();

     if ($is_owner == 0) {
       $ret = ['estado' => 'error'];
       return $ret;
      }

      $comment = new Comment;
      $comment->user_id = $user_id;
      $comment->forum_id = $forum_id;
      $comment->comment = $request->forum_comment_response_comment;
      $comment->edit = '0';

      $saved = $comment->save();

      if ($saved) {
        $comment = Comment::getComment($comment->id);
        $ret = ['estado' => 'ok', 'comment' => $comment];
      }else{
        $ret = ['estado' => 'error'];
      }

      return $ret;
  }
}
