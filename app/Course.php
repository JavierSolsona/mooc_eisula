<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Course extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'user_id', 'description', 'image_route',
  ];

  protected $table = 'courses';

  static public function getCoursesWithoutAuth() {
    return DB::table('courses')
              ->join('users', 'users.id', '=', 'courses.user_id')
              ->select('courses.*', 'users.name as user_name')
              ->get();
  }

  static public function getCoursesWithAuth($id) {
    return DB::table('courses')
              ->join('users', 'users.id', '=', 'courses.user_id')
              ->select('courses.*', 'users.name as user_name',
                DB::raw('
                         (SELECT COUNT(*)
                         FROM signed_ups
                         WHERE signed_ups.user_id = ? AND signed_ups.course_id = courses.id
                         ) as signed'
                       )
              )->setBindings([$id])->get();
  }

  static public function searchWithAuth($search, $id) {
    return DB::table('courses')
              ->join('users', 'users.id', '=', 'courses.user_id')
              ->leftJoin('themes', 'themes.course_id', '=', 'courses.id')
              ->whereRaw("(courses.name like '%".$search."%' or themes.name like '%".$search."%' or users.name like '%".$search."%'
                         or courses.description like '%" . $search ."%' or themes.description like '%" . $search . "%')")
              ->select('courses.*', 'users.name as user_name',
                DB::raw('
                         (SELECT COUNT(*)
                         FROM signed_ups
                         WHERE signed_ups.user_id = ? AND signed_ups.course_id = courses.id
                         ) as signed'
                       )
              )->setBindings([$id])->distinct()->get();
  }

  static public function searchWithoutAuth($search) {
    return DB::table('courses')
              ->join('users', 'users.id', '=', 'courses.user_id')
              ->leftJoin('themes', 'themes.course_id', '=', 'courses.id')
              ->whereRaw("(courses.name like '%".$search."%' or themes.name like '%".$search."%' or users.name like '%".$search."%'
                         or courses.description like '%" . $search ."%' or themes.description like '%" . $search . "%')")
              ->select('courses.*', 'users.name as user_name')
              ->distinct()->get();
  }

  static public function getCourseWithAuth($course_id, $id) {
    return DB::table('courses')
              ->where('courses.id', '=', '?')
              ->select('courses.*',
                DB::raw('
                         (SELECT COUNT(*)
                         FROM signed_ups
                         WHERE signed_ups.user_id = ? AND signed_ups.course_id = courses.id
                         ) as signed'
                       )
              )->setBindings([$id, $course_id])->first();
  }

  static public function coursesNumber() {
    return DB::table('courses')->count();
  }

  static public function coursesNumberByTeacher() {
    return DB::table('users')
          ->join('rols', 'rols.id', '=', 'users.rol_id')
          ->where('rols.name', 'teacher')
          ->select('users.name',
                  DB::raw('
                     (SELECT COUNT(*)
                     FROM courses
                     WHERE courses.user_id = users.id
                     ) as couses'
                   )
            )->get();
  }

  static public function getCoursesHome() {
    return DB::table('courses')
              ->join('users', 'users.id', '=', 'courses.user_id')
              ->select('courses.*', 'users.name as user_name')
              ->orderBy('courses.id', 'desc')
              ->limit(12)
              ->get();
  }
}
