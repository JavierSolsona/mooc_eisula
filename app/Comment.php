<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Comment extends Model
{
  protected $fillable = [
      'user_id', 'forum_id', 'comment', 'edit',
  ];

  protected $table = 'comments';

  static public function getComments($forum_id) {
    return DB::table('comments')
              ->join('users', 'users.id', '=', 'comments.user_id')
              ->where('comments.forum_id', '=', $forum_id)
              ->select('comments.*', 'users.name as user_name')
              ->orderBy('comments.id', 'asc')
              ->get();
  }

  static public function getComment($comment_id) {
    return DB::table('comments')
              ->join('users', 'users.id', '=', 'comments.user_id')
              ->join('forums', 'forums.id', '=', 'comments.forum_id')
              ->join('themes', 'themes.id', '=', 'forums.theme_id')
              ->join('courses', 'courses.id', '=', 'themes.course_id')
              ->where('comments.id', '=', $comment_id)
              ->select('comments.*', 'users.name as user_name', 'courses.user_id as forum_owner')
              ->first();
  }

  static public function getCommentForCourse($comment_id) {
    return DB::table('comments')
              ->join('users', 'users.id', '=', 'comments.user_id')
              ->join('forums', 'forums.id', '=', 'comments.forum_id')
              ->join('themes', 'themes.id', '=', 'forums.theme_id')
              ->join('courses', 'courses.id', '=', 'themes.course_id')
              ->where('comments.id', '=', $comment_id)
              ->select('comments.*', 'users.name as user_name',
                       'themes.name as theme_name', 'courses.user_id as course_owner')
              ->first();
  }
}
