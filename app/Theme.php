<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Theme extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'course_id', 'description',
  ];

  protected $table = 'themes';

  static public function getThemes($course_id) {
    return DB::table('themes')
              ->join('courses', 'courses.id', '=', 'themes.course_id')
              ->where('courses.id', '=', $course_id)
              ->select('themes.*', 'courses.name as course_name')
              ->get();
  }

  static public function getData($theme_id) {
    return DB::table('themes')
              ->join('courses', 'courses.id', '=', 'themes.course_id')
              ->join('users', 'users.id', '=', 'courses.user_id')
              ->where('themes.id', '=', $theme_id)
              ->select('themes.*', 'courses.name as course_name', 'users.name as user_name')
              ->first();
  }

  static public function getThemesWithAuth($course_id, $id) {
    return DB::table('themes')
              ->join('courses', 'courses.id', '=', 'themes.course_id')
              ->where('courses.id', '=', '?')
              ->select('themes.*', 'courses.name as course_name', 'courses.user_id as course_owner', 
                DB::raw('
                       (SELECT COUNT(*)
                       FROM signed_ups
                       WHERE signed_ups.user_id = ? AND signed_ups.course_id = courses.id
                       ) as signed'
                     )
            )->setBindings([$id, $course_id])->get();
  }

}
