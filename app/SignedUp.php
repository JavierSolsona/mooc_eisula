<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SignedUp extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'user_id', 'course_id',
  ];

  protected $table = 'signed_ups';

  static public function getCourseSigned($id) {
    return DB::table('signed_ups')
              ->join('courses', 'courses.id', '=', 'signed_ups.course_id')
              ->where('signed_ups.user_id', '=', $id)
              ->select('courses.*',
                DB::raw('
                       (SELECT users.name
                       FROM users
                       INNER JOIN "courses" ON "courses"."user_id" = "users"."id"
                       WHERE "courses"."id" = "signed_ups"."course_id") as user_name'
                     )
              )->distinct()->get();
  }

  static public function average($courses_number) {
    if ($courses_number == 0) {
      return 0;
    }
    
    return round(DB::table('signed_ups')->count() / $courses_number, 2);
  }

  static public function studentsByCourse() {
    return DB::table('courses')
          ->select('courses.name',
                  DB::raw('
                     (SELECT COUNT(*)
                     FROM signed_ups
                     WHERE signed_ups.course_id = courses.id
                     ) as signed'
                   )
            )->get();
  }
}
