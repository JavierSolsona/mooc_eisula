<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Illuminate\Http\Exceptions\PostTooLargeException) {
          return redirect()->back()->with('error_message', 'El archivo es muy pesado para el servidor');
        }

        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
          return redirect()->route('login')->with('error_message', 'Tú sesión a expirado');
        }

        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
          return redirect()->route('login')->with('error_message', 'Debes iniciar sesión');
        }

        return parent::render($request, $exception);
    }
}
