<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'country_id', 'universityStudent', 'photo', 'rol_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    protected $table = 'users';

    public function rol() {
  		return $this->belongsTo('App\Rol');
  	}

    public function authorizeRoles($roles) {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }

    public function hasAnyRole($roles) {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role) {
        if ($this->rol()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    static public function getConuntries() {
      $countries = DB::table('countries')->select('name', 'id')->pluck('name', 'id');

      return $countries;
    }

    static public function studentsNumber() {
      return DB::table('users')->join('rols', 'rols.id', '=', 'users.rol_id')->where('rols.name', 'student')->count();
    }

    static public function teachersNumber() {
      return DB::table('users')->join('rols', 'rols.id', '=', 'users.rol_id')->where('rols.name', 'teacher')->count();
    }
}
