function changeType(){
	var form = document.InformationForm;
  	var tipo = form.type_id;
  	var file = document.getElementById("div-file");
  	var link = document.getElementById("div-link");

  	if (tipo.value == 2) {
  		link.style.display = null;
  		file.style.display = "none";
  	}else {
  		link.style.display = "none";
  		file.style.display = null;
  	}
}

$(document).ready( function () {
	changeType();
} );
