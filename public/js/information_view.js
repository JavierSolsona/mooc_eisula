function information_view(INFORMATION_ID, USER_ID) {
  var request = new FormData();
  request.append('information_id',INFORMATION_ID);
  request.append('user_id',USER_ID);
      $.ajax({
          url: '/ajax/information/view',
          type: 'post',
          dataType: 'json',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: request,
          processData:false,
          contentType: false,
      })
      .done(function(data) {
          if(data.estado == 'ok'){
            console.log("success");
          }else{
            console.log("error");
          }
      })
      .fail(function() {
        console.log("fail");
      })
      .always(function() {
      });
}
