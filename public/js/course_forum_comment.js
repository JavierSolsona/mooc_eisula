$(document).ready(function($) {
    $('form#forum_comment_response').validate({
        rules:{
            forum_comment_response_name : {
                required: true
            },
            forum_comment_response_comment : {
                required: true
            },
            forum_comment_response_user_id :{
                required: true
            },
            forum_comment_response_forum_id :{
                required: true
            }
        },
        messages:{
            forum_comment_response_name :{
                required:'Un nombre es requerido'
            },
            forum_comment_response_comment :{
                required:'Comentario requerido.'
            },
            forum_comment_response_user_id :{
                required: 'Error en el formulario.'
            },
            forum_comment_response_forum_id :{
                required: 'Error en el formulario.'
            },
        },
        submitHandler:function(form){
            var formData = new FormData($('form#forum_comment_response').get(0));
            $.blockUI({
                message: 'Enviando Respuesta...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } });
            $.ajax({
                url: '/ajax/course/comment/response',
                type: 'post',
                dataType: 'json',
                data: formData,
                processData:false,
                contentType: false,
            })
                .done(function(data) {
                    console.log("success");

                    if(data.estado == 'ok'){
                      $('form#forum_comment_response').get(0).reset();
                      setTimeout($.blockUI({
                          message: 'Comentario Exitoso...',
                          css: {
                              border: 'none',
                              padding: '15px',
                              backgroundColor: '#000',
                              '-webkit-border-radius': '10px',
                              '-moz-border-radius': '10px',
                              opacity: .5,
                              color: '#fff'
                          } }), 2000);

                          var date = new Date(data.comment.created_at);

                          var month = (1 + date.getMonth()).toString();
                          month = month.length > 1 ? month : '0' + month;

                          var day = date.getDate().toString();
                          day = day.length > 1 ? day : '0' + day;

                          var hour = date.getHours().toString();
                          hour = hour.length > 1 ? hour : '0' + hour;

                          var min = date.getMinutes().toString();
                          min = min.length > 1 ? min : '0' + min;

                          var iniT = '<div class="comment-border-bottom" id="comment'+ data.comment.id +'" name="comment' + data.comment.id + '"> <p class="paragraph-margin-bottom"> ' +
                                     'Comentario del tema <span id="comment'+ data.comment.id +'_theme"> ' + data.comment.theme_name + '</span> </p><p class="paragraph-margin-bottom">' +
                                     '<span><i class="fa fa-calendar"></i> '+ day + '-' + month + '-' +  date.getFullYear()  +  ' | <i class="fa fa-clock-o"> </i> ' + hour + ':' + min +
                                     '</span></p> <p class="paragraph-margin-bottom"> <span id="comment'+ data.comment.id +'_user">' + data.comment.user_name + '</span> ';

                          var medT = '';

                          if ( data.comment.user_id == data.comment.course_owner) {
                            medT = '<small>Profesor</small>';
                          }

                          var endT = '</p><p class="paragraph-margin-bottom" id="comment'+ data.comment.id +'_comment"> <span>' + nl2br(data.comment.comment) + '</span> </p>' +
                                     '<p class="paragraph-margin-bottom not-display" id="comment'+ data.comment.id +'_forum" >' + btoa(data.comment.forum_id) + '</p>' +
                                     '<a  onclick="form_response_open(' + data.comment.id + ')">Responder al foro</a>' +
                                      '<a  onclick="form_edit_open(' + data.comment.id + ')"> Editar</a><a  onclick="delete_comment(' + data.comment.id + ')"> Eliminar</a></div>';
                          $('#comment-box').append(iniT+medT+endT);
                          document.getElementById("forum_comment_response").style.display = "none";
                    }else{
                      setTimeout($.blockUI({
                          message: 'Error Enviando el Comentario...',
                          css: {
                              border: 'none',
                              padding: '15px',
                              backgroundColor: '#000',
                              '-webkit-border-radius': '10px',
                              '-moz-border-radius': '10px',
                              opacity: .5,
                              color: '#fff'
                          } }), 2000);
                    }
                })
                .fail(function() {
                  setTimeout($.blockUI({
                      message: 'Error Enviando el Comentario...',
                      css: {
                          border: 'none',
                          padding: '15px',
                          backgroundColor: '#000',
                          '-webkit-border-radius': '10px',
                          '-moz-border-radius': '10px',
                          opacity: .5,
                          color: '#fff'
                      } }), 2000);
                })
                .always(function() {
                    setTimeout($.unblockUI, 1000);

                });
        }
    });

    $('form#forum_comment_update').validate({
        rules:{
            forum_comment_name_update : {
                required: true
            },
            forum_comment_comment_update : {
                required: true
            },
            comment_id_update :{
                required: true
            }
        },
        messages:{
            forum_comment_name_update :{
                required:'Un nombre es requerido'
            },
            forum_comment_comment_update :{
                required:'Comentario requerido.'
            },
            comment_id_update :{
                required: 'Error en el formulario.'
            },
        },
        submitHandler:function(form){
            var formData = new FormData($('form#forum_comment_update').get(0));
            $.blockUI({
                message: 'Enviando Comentario...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } });
            $.ajax({
                url: '/ajax/course/comment/editar',
                type: 'post',
                dataType: 'json',
                data: formData,
                processData:false,
                contentType: false,
            })
                .done(function(data) {
                    console.log("success");

                    if(data.estado == 'ok'){
                      $('form#forum_comment_update').get(0).reset();
                      setTimeout($.blockUI({
                          message: 'Comentario Editado...',
                          css: {
                              border: 'none',
                              padding: '15px',
                              backgroundColor: '#000',
                              '-webkit-border-radius': '10px',
                              '-moz-border-radius': '10px',
                              opacity: .5,
                              color: '#fff'
                          } }), 2000);

                          $('#comment'+data.comment.id).empty();

                          var date = new Date(data.comment.created_at);

                          var month = (1 + date.getMonth()).toString();
                          month = month.length > 1 ? month : '0' + month;

                          var day = date.getDate().toString();
                          day = day.length > 1 ? day : '0' + day;

                          var hour = date.getHours().toString();
                          hour = hour.length > 1 ? hour : '0' + hour;

                          var min = date.getMinutes().toString();
                          min = min.length > 1 ? min : '0' + min;

                          var iniT = '<p class="paragraph-margin-bottom"> Comentario del tema <span id="comment'+ data.comment.id +'_theme"> ' + data.comment.theme_name + '</span> </p><p class="paragraph-margin-bottom">' +
                                     '<span><i class="fa fa-calendar"></i> '+ day + '-' + month + '-' +  date.getFullYear()  +  ' | <i class="fa fa-clock-o"> </i> ' + hour + ':' + min +
                                     '</span></p> <p class="paragraph-margin-bottom"> <span id="comment'+ data.comment.id +'_user">' + data.comment.user_name + '</span> ';

                          var medT = '';

                          if ( data.comment.user_id == data.comment.course_owner) {
                            medT = '<small>Profesor</small>';
                          }

                          var endT =  '</p><p class="paragraph-margin-bottom" id="comment'+ data.comment.id +'_comment"> <span>' + nl2br(data.comment.comment) + '</span> </p>' +
                                     '<p class="paragraph-margin-bottom not-display" id="comment'+ data.comment.id +'_forum" >' + btoa(data.comment.forum_id) + '</p>' +
                                     '<p class="paragraph-margin-bottom"> <span> Editado</span> </p> <a  onclick="form_response_open(' + data.comment.id + ')">Responder al foro</a>' +
                                      '<a  onclick="form_edit_open(' + data.comment.id + ')"> Editar</a><a  onclick="delete_comment(' + data.comment.id + ')"> Eliminar</a>';
                          $('#comment'+data.comment.id).append(iniT+medT+endT);
                          document.getElementById("forum_comment_update").style.display = "none";

                    }else{
                      setTimeout($.blockUI({
                          message: 'Error Editando el Comentario...',
                          css: {
                              border: 'none',
                              padding: '15px',
                              backgroundColor: '#000',
                              '-webkit-border-radius': '10px',
                              '-moz-border-radius': '10px',
                              opacity: .5,
                              color: '#fff'
                          } }), 2000);
                    }
                })
                .fail(function() {
                  setTimeout($.blockUI({
                      message: 'Error Editando el Comentario...',
                      css: {
                          border: 'none',
                          padding: '15px',
                          backgroundColor: '#000',
                          '-webkit-border-radius': '10px',
                          '-moz-border-radius': '10px',
                          opacity: .5,
                          color: '#fff'
                      } }), 2000);
                })
                .always(function() {
                    setTimeout($.unblockUI, 1000);
                });
        }
    });

});

function form_response_open(COMMENT_ID){
  document.getElementById("forum_comment_update").style.display = "none";
  document.getElementById("forum_comment_response").style.display = null;
  $('#forum_comment_response_theme').empty();
  $('#forum_comment_response_theme').append($.trim($('#comment'+ COMMENT_ID + '_theme').text()));
  $('#forum_comment_response_forum_id').val($.trim($('#comment'+ COMMENT_ID +'_forum').text()));
  $('#forum_comment_response_comment').val('Comentario de: ' + $.trim($('#comment'+ COMMENT_ID + '_user').text())+ '\n' +
                                           $.trim($('#comment'+ COMMENT_ID +'_comment').text()) + '\n\n' +
                                          'Respuesta: ');
  $('html,body').animate({scrollTop: $("#forum_comment_response").offset().top},'slow');
}

function cancel_response(){
  $('form#forum_comment_response').get(0).reset();
  document.getElementById("forum_comment_response").style.display = "none";
}

function form_edit_open(COMMENT_ID){
  document.getElementById("forum_comment_response").style.display = "none";
  document.getElementById("forum_comment_update").style.display = null;
  $('#forum_comment_name_update').val($.trim($('#comment'+ COMMENT_ID + '_user').text()));
  $('#forum_comment_comment_update').val($.trim($('#comment'+ COMMENT_ID +'_comment').text()));
  $('#comment_id_update').val(btoa(COMMENT_ID));
  $('html,body').animate({scrollTop: $("#forum_comment_update").offset().top},'slow');
}

function cancel_update(){
  $('form#forum_comment_update').get(0).reset();
  document.getElementById("forum_comment_update").style.display = "none";
}

function delete_comment(COMMENT_ID){
  if (confirm("¿Seguro de eliminar comentario?")) {
    var request = new FormData();
    request.append('comment_id', btoa(COMMENT_ID));
    $.blockUI({
        message: 'Eliminadno Comentario...',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        $.ajax({
            url: '/ajax/comment/eliminar',
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: request,
            processData:false,
            contentType: false,
        })
        .done(function(data) {
            if(data.estado == 'ok'){
              $('#comment'+data.id).remove();
              setTimeout($.blockUI({
                message: 'Comentario Eliminado...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } }), 2000);
            }else{
              setTimeout($.blockUI({
                message: 'Error Eliminando Comentario...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } }), 2000);
            }
        })
        .fail(function() {
          setTimeout($.blockUI({
            message: 'Error Eliminando Comentario...',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } }), 2000);
        })
        .always(function() {
            setTimeout($.unblockUI, 1000);
        });
  }else{
    return false;
  }
}

function nl2br (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
