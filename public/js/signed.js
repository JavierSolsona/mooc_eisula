function signedUp(COURSE_ID, USER_ID) {
  if (confirm("¿Seguro que deseas inscribirte en este curso?")) {
    var request = new FormData();
    request.append('course_id',COURSE_ID);
    request.append('user_id',USER_ID);
    $.blockUI({
        message: 'Inscribiendo...',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        $.ajax({
            url: '/ajax/signed_up',
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: request,
            processData:false,
            contentType: false,
        })
        .done(function(data) {
            if(data.estado == 'ok'){
              console.log("success");
              $('#'+COURSE_ID).empty();
              var endT = '<button type="button" name="button" class="btn btn-danger btn-style-table" onclick="remove('+COURSE_ID+','+USER_ID + ')">Retirarse</button>';
              $('#'+COURSE_ID).append(endT);
              setTimeout($.blockUI({
                message: 'Inscripción Exitosa...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } }), 2000);
            }else{
              setTimeout($.blockUI({
                message: 'Error Inscribiendo...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } }), 2000);
            }
        })
        .fail(function() {
          setTimeout($.blockUI({
            message: 'Error Inscribiendo...',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } }), 2000);
        })
        .always(function() {
            setTimeout($.unblockUI, 1000);
        });
  }
}

function remove(COURSE_ID, USER_ID) {
  if (confirm("¿Seguro que deseas retirarte de este curso?")) {
    var request = new FormData();
    request.append('course_id',COURSE_ID);
    request.append('user_id',USER_ID);
    $.blockUI({
        message: 'Retirando...',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        $.ajax({
            url: '/ajax/remove',
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: request,
            processData:false,
            contentType: false,
        })
        .done(function(data) {
          if(data.estado == 'ok'){
            console.log("success");
            $('#'+COURSE_ID).empty();
            var endT = '<button type="button" name="button" class="btn btn-primary btn-style-table" onclick="signedUp('+COURSE_ID+','+USER_ID + ')">Inscribir</button>';
            $('#'+COURSE_ID).append(endT);
            setTimeout($.blockUI({
              message: 'Retirado Exitosamente...',
              css: {
                  border: 'none',
                  padding: '15px',
                  backgroundColor: '#000',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .5,
                  color: '#fff'
              } }), 2000);
          }else{
            setTimeout($.blockUI({
              message: 'Error Retirando...',
              css: {
                  border: 'none',
                  padding: '15px',
                  backgroundColor: '#000',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .5,
                  color: '#fff'
              } }), 2000);
          }
        })
        .fail(function() {
          setTimeout($.blockUI({
            message: 'Error Retirando...',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } }), 2000);
        })
        .always(function() {
            setTimeout($.unblockUI, 1000);
        });
  }
}

function signedUp2(COURSE_ID, USER_ID) {
  if (confirm("¿Seguro que deseas inscribirte en este curso?")) {
    var request = new FormData();
    request.append('course_id',COURSE_ID);
    request.append('user_id',USER_ID);
    $.blockUI({
        message: 'Inscribiendo...',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        $.ajax({
            url: '/ajax/signed_up',
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: request,
            processData:false,
            contentType: false,
        })
        .done(function(data) {
            if(data.estado == 'ok'){
              setTimeout($.blockUI({
                message: 'Inscripción Exitosa...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } }), 2000);
                location.reload(); 
            }else{
              setTimeout($.blockUI({
                message: 'Error Inscribiendo...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                } }), 2000);
            }
        })
        .fail(function() {
          setTimeout($.blockUI({
            message: 'Error Inscribiendo...',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } }), 2000);
        })
        .always(function() {
            setTimeout($.unblockUI, 1000);
        });
  }
}

function remove2(COURSE_ID, USER_ID) {
  if (confirm("¿Seguro que deseas retirarte de este curso?")) {
    var request = new FormData();
    request.append('course_id',COURSE_ID);
    request.append('user_id',USER_ID);
    $.blockUI({
        message: 'Retirando...',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        $.ajax({
            url: '/ajax/remove',
            type: 'post',
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: request,
            processData:false,
            contentType: false,
        })
        .done(function(data) {
          if(data.estado == 'ok'){
            setTimeout($.blockUI({
              message: 'Retirado Exitosamente...',
              css: {
                  border: 'none',
                  padding: '15px',
                  backgroundColor: '#000',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .5,
                  color: '#fff'
              } }), 2000);
              location.reload();
          }else{
            setTimeout($.blockUI({
              message: 'Error Retirando...',
              css: {
                  border: 'none',
                  padding: '15px',
                  backgroundColor: '#000',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .5,
                  color: '#fff'
              } }), 2000);
          }
        })
        .fail(function() {
          setTimeout($.blockUI({
            message: 'Error Retirando...',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            } }), 2000);
        })
        .always(function() {
            setTimeout($.unblockUI, 1000);
        });
  }
}
