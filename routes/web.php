<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
Route::get('ingresar', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('ingresar', 'Auth\LoginController@login');
Route::post('salir', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('registar', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('registar', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('recobrar/contrasena', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('recobrar/correo', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('recobrar/contrasena/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('recobrar/contrasena', 'Auth\ResetPasswordController@reset');

Route::get('/', ['as' => '/', 'uses' => 'HomeController@index']);
Route::get('/cursos/ver', ['as' => 'course.showAll', 'uses' => 'CourseController@showAll']);
Route::post('/cursos/buscar', ['as' => 'course.search', 'uses' => 'CourseController@search']);
Route::get('/{course_name}/temas/ver/{course_id}', ['as' => 'theme.show', 'uses' => 'ThemeController@show']);
Route::get('/manual/usuario', ['as' => 'user.manual.index', 'uses' => 'UserController@manualIndex']);
Route::get('/manual/usuario/{video}', ['as' => 'user.manual.show', 'uses' => 'UserController@manualShow']);

Route::group(['middleware' => ['auth']], function () {
  Route::get('/perfil/editar/{name}/{id}', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
  Route::put('/perfil/editar', ['as' => 'user.update', 'uses' => 'UserController@update']);
  Route::get('/perfil/eliminar/{name}/{id}', ['as' => 'user.delete', 'uses' => 'UserController@delete']);
  Route::get('/profesor/crear/', ['as' => 'teacher.create', 'uses' => 'UserController@createTeacher']);
  Route::post('/profesor/crear/', ['as' => 'teacher.store', 'uses' => 'UserController@storeTeacher']);
  Route::get('/cursos/{name}/{id}', ['as' => 'course.index', 'uses' => 'CourseController@index']);
  Route::get('/cursos/crear/{name}/{id}', ['as' => 'course.create', 'uses' => 'CourseController@create']);
  Route::post('/cursos/crear/', ['as' => 'course.store', 'uses' => 'CourseController@store']);
  Route::get('/cursos/editar/{name}/{course_id}', ['as' => 'course.edit', 'uses' => 'CourseController@edit']);
  Route::put('/cursos/editar/', ['as' => 'course.update', 'uses' => 'CourseController@update']);
  Route::get('/cursos/eliminar/{name}/{course_id}', ['as' => 'course.delete', 'uses' => 'CourseController@delete']);
  Route::get('/{course_name}/temas/{user_name}/{user_id}/{course_id}', ['as' => 'theme.index', 'uses' => 'ThemeController@index']);
  Route::get('/{course_name}/temas/crear/{user_name}/{user_id}/{course_id}', ['as' => 'theme.create', 'uses' => 'ThemeController@create']);
  Route::post('/temas/crear/', ['as' => 'theme.store', 'uses' => 'ThemeController@store']);
  Route::get('/{course_name}/temas/editar/{user_name}/{user_id}/{theme_id}', ['as' => 'theme.edit', 'uses' => 'ThemeController@edit']);
  Route::put('/temas/editar/', ['as' => 'theme.update', 'uses' => 'ThemeController@update']);
  Route::get('/{course_name}/temas/eliminar/{user_name}/{user_id}/{theme_id}', ['as' => 'theme.delete', 'uses' => 'ThemeController@delete']);
  Route::get('/{theme_name}/informacion/{user_name}/{user_id}/{theme_id}', ['as' => 'information.index', 'uses' => 'InformationController@index']);
  Route::get('/{theme_name}/informacion/crear/{user_name}/{user_id}/{theme_id}', ['as' => 'information.create', 'uses' => 'InformationController@create']);
  Route::post('/informacion/crear/', ['as' => 'information.store', 'uses' => 'InformationController@store']);
  Route::get('/{theme_name}/informacion/editar/{user_name}/{user_id}/{information_id}', ['as' => 'information.edit', 'uses' => 'InformationController@edit']);
  Route::put('/informacion/editar/', ['as' => 'information.update', 'uses' => 'InformationController@update']);
  Route::get('/{theme_name}/informacion/eliminar/{user_name}/{user_id}/{information_id}', ['as' => 'information.delete', 'uses' => 'InformationController@delete']);
  Route::post('/ajax/signed_up', ['as'=> 'signedUp.create', 'uses'=> 'SignedUpController@signedUp']);
  Route::post('/ajax/remove', ['as'=> 'signedUp.delete', 'uses'=> 'SignedUpController@remove']);
  Route::get('/cursos/inscritos/{name}/{id}', ['as' => 'signedUp.signed', 'uses' => 'SignedUpController@signed']);
  Route::get('/{theme_name}/ver/informacion/{user_id}/{theme_id}', ['as' => 'information.show', 'uses' => 'InformationController@show']);
  Route::get('/informacion/ver/video{user_id}/{information_id}', ['as' => 'information.showVideo', 'uses' => 'InformationController@showVideo']);
  Route::get('/{theme_name}/foro/ver/{user_id}/{theme_id}', ['as' => 'forum.index', 'uses' => 'ForumController@index']);
  Route::post('/ajax/comment/crear', ['as' => 'comment.create', 'uses' => 'CommentController@store']);
  Route::post('/ajax/comment/editar', ['as' => 'comment.update', 'uses' => 'CommentController@update']);
  Route::post('/ajax/comment/eliminar', ['as' => 'comment.delete', 'uses' => 'CommentController@delete']);
  Route::get('/respaldo/crear', ['as' => 'backup.create', 'uses' => 'UserController@createBackup']);
  Route::post('/respaldo/recuperar', ['as' => 'backup.recover', 'uses' => 'UserController@recoverBackup']);
  Route::post('/ajax/information/view', ['as' => 'information.view', 'uses' => 'InformationController@informationView']);
  Route::get('/{course_name}/curso/foro/ver/{user_id}/{course_id}', ['as' => 'forum.course', 'uses' => 'ForumController@courseForum']);
  Route::post('/ajax/course/comment/editar', ['as' => 'comment.update.course', 'uses' => 'CommentController@courseUpdate']);
  Route::post('/ajax/course/comment/response', ['as' => 'comment.response.course', 'uses' => 'CommentController@courseResponse']);
  Route::get('/estadisticas', ['as' => 'user.stats', 'uses' => 'UserController@stats']);
  Route::post('/ajax/comment/crearResponse', ['as' => 'comment.response', 'uses' => 'CommentController@response']);
  Route::get('/imagenes', ['as' => 'slider.index', 'uses' => 'SliderController@index']);
  Route::get('/imagenes/crear', ['as' => 'slider.create', 'uses' => 'SliderController@create']);
  Route::post('/imagenes/crear', ['as' => 'slider.store', 'uses' => 'SliderController@store']);
  Route::get('/imagenes/editar/{slider_id}', ['as' => 'slider.edit', 'uses' => 'SliderController@edit']);
  Route::put('imagenes/editar', ['as' => 'slider.update', 'uses' => 'SliderController@update']);
  Route::get('/imagenes/eliminar/{slider_id}', ['as' => 'slider.delete', 'uses' => 'SliderController@delete']);
});
